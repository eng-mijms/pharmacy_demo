from rest_framework import serializers
from rest_framework import permissions


#base64 image class to read image files as ajax
class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
        # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]
            # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension



#main permissions class
class AppPermissions(permissions.BasePermission):
    class Mysettings:
        modelname = 'Account'
        user = 'user'
        group = 'group'
        permissions = 'permissions'
        get = 'get'
        add = 'add'
        edit = 'edit'
        delete = 'delete'

    # @property
    def permission(self, request, view, account=None):
        try:
            if(request.user.is_admin):
                return True
            permission = getattr(getattr(getattr(request, self.Mysettings.user), self.Mysettings.group), self.Mysettings.permissions).get(modelname=self.Mysettings.modelname)
            if(request.method == 'GET'):
                return getattr(permission, self.Mysettings.get)
            if(request.method == 'POST'):
                return getattr(permission, self.Mysettings.add)
            if(request.method == 'PUT'):
                return getattr(permission, self.Mysettings.edit)
            if(request.method == 'DELETE'):
                return getattr(permission, self.Mysettings.delete)

        except Exception:
            return False
        return False
