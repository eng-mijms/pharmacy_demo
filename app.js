define([],function(){
  // debugger
  /**
  * pharmacy Module
  *
  * this is the Main pharmacy application for angularjs
  */
  angular.module('pharmacy', ['ui.router']).config(function($stateProvider) {
    $stateProvider.state('home',{
        url:'',
        templateUrl:'/static/apps/templates/html/home.html'
    });

});
})
