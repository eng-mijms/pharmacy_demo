(function(){
	angular.module('ChartsDirectives', []).directive('testd3', [function () {
		return {
			restrict: 'AE',
			link: function (scope, el, attr) {
				d3.select(el[0]).append("svg").attr('width', 50).attr('height', 50).append('rect').attr('width', 50).attr('height', 50).attr('cx', 0).attr('cy', 0).attr('fill', 'RED');
				d3.select(el[0]).append("svg").attr("width", 50).attr("height", 50).append("circle").attr("cx", 25).attr("cy", 25).attr("r", 25).style("fill", "purple");
				var p = d3.select(el[0]).selectAll("p")
				  .data([7,8,9])
				  .enter()
				  .append("p")
				  .text(function(d,i){
				  	return "this is "+d+" and " +i;
				  });

				  //The data for our line
				 var lineData = [ { "x": 1,   "y": 5},  { "x": 20,  "y": 20},
				                  { "x": 40,  "y": 10}, { "x": 60,  "y": 40},
				                  { "x": 80,  "y": 5},  { "x": 100, "y": 60}];
				 
				 //This is the accessor function we talked about above
				 var lineFunction = d3.svg.line()
				                          .x(function(d) { return d.x; })
				                          .y(function(d) { return d.y; })
				                         .interpolate("basis");
				
				//The SVG Container
				var svgContainer = d3.select(el[0]).append("svg")
				                                    .attr("width", 200)
				                                    .attr("height", 200);
				
				//The line SVG Path we draw
				var lineGraph = svgContainer.append("path")
				                            .attr("d", lineFunction(lineData))
				                            .attr("stroke", "blue")
				                            .attr("stroke-width", 2)
				                            .attr("fill", "none");

                   var spaceCircles = [30,70,100];
                   var svgContainer = d3.select(el[0]).append("svg").attr('width', 200).attr('height', 200);
                   var circles = svgContainer.selectAll("circle").data(spaceCircles).enter().append("circle");
                   var cattr = circles
                       .attr('cx', function(d){return d;})
                       .attr('cy', function(d){return d;})
                       .attr('r', 30)
                       .style('fill', function(d){
                       	var returnColor;
                         if (d === 30) { returnColor = "green";
	                     } else if (d === 70) { returnColor = "purple";
                         } else if (d === 110) { returnColor = "red"; }
                         return returnColor;
                       });



				 	circleRadii = [40, 20, 10]
 					var svgContainer = d3.select(el[0]).append("svg")
                                     .attr("width", 600)
                                     .attr("height", 100);
					 
				 	var circles = svgContainer.selectAll("circle")
					                           .data(circleRadii)
					                           .enter()
					                          .append("circle")

					var circleAttributes = circles
					                       .attr("cx", 50)
					                      .attr("cy", 50)
					                       .attr("r", function (d) { return d; })
					                       .style("fill", function(d) {
					                         var returnColor;
					                         if (d === 40) { returnColor = "green";
					                         } else if (d === 20) { returnColor = "purple";
					                         } else if (d === 10) { returnColor = "red"; }
					                         return returnColor;
                       						});

			}
		};
	}])
})();