define(['../services/crudserv'],function(){
	angular.module('CrudModels', ['CrudServ','ui.bootstrap','infinite-scroll']).controller('CrudCtrl', ['Cruds','$stateParams', CrudCtrl]);
	function CrudCtrl (Cruds, $stateParams) {
		cu = this
		cu.modelactive = true;
		cu.lists={}
		cu.loadings={}
      // debugger
      cu.settings = $stateParams.settings
		cu.permissions = $stateParams.permissions
		// cu.item = {}
		// console.log(cu.settings);
		cu.list = []
		cu.listloading = false;
		cu.createview=false;
		cu.queryset = Cruds.Models(cu.settings.url).query(function() {
			cu.list = cu.list.concat(cu.queryset.results)
		});
		cu.getvalue = function(item,string,callback){
         // debugger
         // console.log(user);
         var temp = Cruds.ObjectFromString(item,string);
         if(callback){
            callback()
         }
         // debugger
         return temp;
         // var o = angular.copy(item)
         // var temp = string.split('.');
         // for(var i=0,n=temp.length;i<n;i++){
         //    var k =temp[i];
         //    if(k in o){
         //       o = o[k];
         //    }else{
         //       return;
         //    }
         // }
         // return o
      }
		cu.scrolldata = function() {
   			if(cu.queryset.next && !cu.listloading){
   				cu.listloading = true;
   				Cruds.DefaultGet(cu.queryset.next).then(function(data){
   					// console.log(data);
   					cu.queryset = data;
   					cu.listloading = false
   					cu.list = cu.list.concat(cu.queryset.results)
   				})
   			}
   		}

   		cu.selectarray = function(field){
   			// console.log(field.url);
   			if(!cu.loadings[field.name] && field.url){
	   			cu.loadings[field.name] = true
	   			Cruds.DefaultGet(field.url).then(function(data){
	   				// console.log(data);
	   				cu.loadings[field.name] = false
	   				field.url = data.next
	   				if(!!cu.lists[field.name]){
	   					cu.lists[field.name]=cu.lists[field.name].concat(data.results)
	   				}else{
						cu.lists[field.name] = data.results;	   					
	   				}
	   				// console.log(cu.lists[field.name]);
	   			})
   			}


   		}

   		cu.formsubmit= function(){
   			console.log(cu.item);
            // console.log(cu.item.image);
   			// Item = new Cruds(cu.settings.url);
   			cu.item;
   			Cruds.Models(cu.settings.url).save(cu.item,function(responce){
   				console.log(responce);
   				cu.item = {};
   				cu.list.push(responce);
   				cu.modelactive = true
   			},function(responce){
   				console.log(responce);
   				cu.test = responce.data
   			})
   		}

   		cu.edititem = function(item){
   			var olditem = angular.copy(item)
   			Cruds.EditModal({settings:cu.settings,lists:cu.lists,selectarray:cu.selectarray,edititem:item}).result.then(function(edititem){
   				// console.log(edititem);
   				Cruds.Models(cu.settings.url).update({id:item.id},item)
   			},function(){
   				// console.log(olditem);
   				index = cu.list.indexOf(item);
   				// console.log(index);
   				cu.list.splice(index, 1)
   				cu.list.splice(index, 0,olditem)
   				
   				console.log('dismiss');
   			})
   		}
   		cu.removeditem = function(item){
   			Cruds.DeleteModal({settings:cu.settings,item:item}).result.then(function(edititem){
   				// console.log(edititem);
   				Cruds.Models(cu.settings.url).delete({id:item.id});
				index = cu.list.indexOf(item);
   				// console.log(index);
   				cu.list.splice(index, 1)	
   			},function(){
   				// console.log(olditem);
   				console.log('dismiss');
   			})
   		}
	}
});