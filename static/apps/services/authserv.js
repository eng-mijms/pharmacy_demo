define([],function(){
	// debugger
	angular.module('AuthServ', ['ngCookies']).factory('AuthUser', ['$http','$cookies',Authentication]);
	function Authentication($http,$cookies) {
		function LogIn (User,url) {
			console.log(User);
			return $http.post(url,User);
		}

		function GetAuthAccount () {
			if(!$cookies.authenticateuseraccount){
				return;
			}
			return JSON.parse($cookies.authenticateuseraccount)
		}

		function GetRealAccount(url) {
			return $http.post(url,{'data':false});
		}

		function AuthCheck () {
			return !!$cookies.authenticateuseraccount
		}

		function SetAuthAccount (account) {
			$cookies.authenticateuseraccount = JSON.stringify(account);
		}

		function UnAuthenticate () {
			delete $cookies.authenticateuseraccount
		}

		function LogOut(url){
			console.log(url);
			return $http.post(url)
		}


	
		return {
			LogIn:LogIn,
			GetAuthAccount:GetAuthAccount,
			AuthCheck:AuthCheck,
			SetAuthAccount:SetAuthAccount,
			UnAuthenticate:UnAuthenticate,
			LogOut:LogOut,
			GetRealAccount: GetRealAccount,
		};
	}
});