define(['./crudserv'],function(){
	angular.module('DashBoardServ', ['CrudServ']).factory('Board', ['Cruds','$rootScope',Board])

	function Board(Cruds,$rootScope){
		var finish = {};
		var loading = {};
		var instanceloading = {};
		var lists = {};
		function loadingdata(url,name,callback){
			if(!finish[name]){
				console.log(url);
				if(!loading[name]){
					loading[name] = true;
					lists[name] = [];
					// loading[name].url = url;
				}
				setTimeout(function(){
					if(!instanceloading[name]){
						instanceloading[name] = true;
						Cruds.DefaultGet(url).then(function(data){
							instanceloading[name]=false;
							lists[name] = lists[name].concat(data.results);
							if(data.next){
								console.log('loadingdata');
								loadingdata(data.next,name,function(d){
									callback(d)
								})
							}else{
								callback(lists[name]);
								finish[name] = true;
								$rootScope.$broadcast('dashboardfinish:'+name, lists[name]);
								console.log(lists);
							}
						})
					}
				},0)
			}else{
				callback(lists[name])
			}

		}

		return {
			loadingdata: loadingdata,
			loading:loading,
			finish: finish,
		};
	
	}
});