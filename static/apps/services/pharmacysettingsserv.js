define([],function(){
	// debugger;
	angular.module('PharmacySettingsServ', []).provider('urlSettings', [function () {
	    var url = ''
	    var templateUrl = 'static/app'
	    var $stateProviderRef;
	    var name;
	    var AuthCheck = {url:'/auth/api/v1/auth/check/'}

	    return {
	      seturl: function(value){
	        url = value;
	        // debugger;
	      },
	      setAuthCheckurl: function(url){
	      	// debugger
	      	AuthCheck.url = url;
	      },
	      setname: function(value){
	        name = value;
	        // debugger;
	      },
	      setStateProvider: function(value){
	        $stateProviderRef = value;
	      },
	      $get: function() {
	        // debugger;
	        return {
	          url: url,
	          templateUrl: templateUrl,
	          $stateProviderRef: $stateProviderRef,
			  name: name,
			  AuthCheck:AuthCheck

	        };
	      }

	    }

	  }])
});