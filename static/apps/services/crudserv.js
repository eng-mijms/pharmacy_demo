define(['./pharmacysettingsserv'],function(){
	angular.module('CrudServ', ['ngResource','ui.bootstrap','PharmacySettingsServ']).factory('Cruds', ['$resource','$http','$modal','$filter','urlSettings',cruds])
	function cruds ($resource,$http,$modal,$filter,urlSettings) {
        var templateurl = urlSettings.templateUrl;
		function Models(url){
			return $resource(url,{},{
				query: {method:'GET', isArray:false},
				'update':{method:'PUT'}
			});
		}
		function DefaultGet(url){
			return $http.get(url).then(function (data, status, headers, config) {
				return data.data;
			},function (data, status, headers, config) {
				return data.data;
			})
		}

		function EditModal(cu) {
            var modalInstance = $modal.open({
                templateUrl: templateurl+'/templates/html/cruds/edit.html',
                backdrop: 'static',
                controller: function($scope, $modalInstance, $sce, cu) {
                	// console.log(cu.edititem['group']);
                	var olditem = cu.edititem
                    $scope.cu = cu
                    $scope.searching = function(element, list,field){
                    	console.log(field);
                        if(element){
                    	   return $filter('filter')(list,element)[0][field]
                        }else{
                            return
                        }
                    }
                    $scope.close = function() {
                    	// $scope.cu.edititem = olditem;
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.save = function() {
                      $modalInstance.close($scope.cu.edititem);
                    };
                },
                size: 'lg',
                resolve: {
                    cu: function() {
                        return cu;
                    },
                   

                }
            });
            return modalInstance;

        }



        function DeleteModal(item) {
            var modalInstance = $modal.open({
                templateUrl: templateurl+'/templates/html/cruds/delete.html',
                backdrop: 'static',
                controller: function($scope, $modalInstance, $sce, cu) {
                	// console.log(cu.edititem['group']);
                    $scope.cu = cu
                    // console.log($scope.cu);
                    // console.log($scope.cu);
                    $scope.close = function() {
                    	// $scope.cu.edititem = olditem;
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.save = function() {
                      $modalInstance.close(true);
                    };
                },
                size: 'lg',
                resolve: {
                    cu: function() {
                        return item;
                    },
                   

                }
            });
            return modalInstance;

        }

        function ObjectFromString(item,string){
            // debugger;
            if(!item){
                return
            }
            var o = angular.copy(item)
            var temp = string.split('.');
            for(var i=0,n=temp.length;i<n;i++){
                var k =temp[i];
                if(o instanceof Array){
                    // debugger;
                    var arraytemp = k.split('=');
                    if(arraytemp[0]=="filter"){
                        var tempo = $filter('filter')(o,arraytemp[1])[0];
                        if(tempo){
                            // debugger
                            o = tempo
                        }else{
                            return;
                        }

                    }else{
                        var tempnum = o.indexOf(k);
                        if(tempnum>-1){
                            o = o[tempnum];
                        }else{
                            return
                        }
                    }
                }else{
                    // debugger
                   if(k in o){
                       o = o[k];
                    }else{
                       return;
                    }
                }

            }
            return o
          }
	
		return {
			Models:Models,
			DefaultGet:DefaultGet,
			EditModal:EditModal,
			DeleteModal:DeleteModal,
            ObjectFromString: ObjectFromString
		};
	}
});