define(['./ngCrud/ngCrud/app','./auth/auth/app'], function(){
    /**
    * test Module
    *
    * Description
    */
    angular.module('pharmacy', ['ngCrud', 'authApp']).config(['$locationProvider', '$stateProvider','$resourceProvider', 'crudSettingsProvider', 'authSettingsProvider',
        function ($locationProvider, $stateProvider, $resourceProvider, crudSettingsProvider, authSettingsProvider ) {
            // debugger
            // authSettingsProvider.mainState = "test";
        $resourceProvider.defaults.stripTrailingSlashes = false;
        // crudSettingsProvider.setRootUrl('/static/')
        // debugger
        var main = crudSettingsProvider.getMainUrl();

        $stateProvider.state('home',{
            url:'/',
            controller: 'CrudCtrl as cu',
            templateUrl: main,
            title: 'home',
            params:{settings:setting}
        }).state('login',{
            url:'/login',
            title: 'login',
            controller: 'userCtrl as us',
            templateUrl:'/static/apps/auth/auth/template/html/signin.html',
        })


        $locationProvider.html5Mode(true);
    }]).
    run(['$state','$http', '$rootScope', 'authServ', 'authSettings', function ($state, $http, $rootScope, authServ, authSettings) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            debugger
            if(!$rootScope.User && toState.name != authSettings.loginState){
                event.preventDefault();
            }
            $rootScope.pageTitle = toState.title;
            authServ.checkAuth().then(function (data) {
                if(data.user){
                    $rootScope.User = data.user;
                    if(toState.name == authSettings.loginState){
                        $state.go(authSettings.mainState)
                    }else{
                        $state.go(toState.name)
                    }

                }else{
                    $state.go(authSettings.loginState);
                }

            }, function(data){
                if(!toState.allAccess){
                    $state.go(authSettings.loginState)
                }
            })
        })
    }]).provider('pharmacySettings', [function () {

      this.name = 'pharmacy';
      this.rootUrl = '/';
      this.$get = [function() {
        return {
            name: this.name,
            rootUrl: this.rootUrl
        };
      }];
    }]);


    var setting = {
        title:"User",
        index:"Num",
        url: '/inventory/api/v1/products/:id/',
        views:[
            {
                head:"name",
                view:"fullname",
            },{
                head:"ID",
                view:"id",
            },
            {
                head:"Email",
                view:"email",
            },
            {
                head:"ID+100",
                expression: function(item){
                    return 7;
                },
            }
        ],
        fields:[
        {
            label: "User Name",
            type: "text",
            placeholder: "Please Enter your Prefered user name",
            model: "username",
            required:true,
            edit:true

        },
        {
            label: "password",
            type: "password",
            // label: "User Name",
            placeholder: "Please Enter your Password",
            model: "password",
        },
        {
            label: "Description",
            type: "textarea",
            row: 10,
            col: 9,
            placeholder: "Please Enter User Description",
            model: "desc",
            required:true,

        },
        {
            label: "Group",
            type: "select",
            placeholder: "Please Enter User Group",
            model: "group_id",
            url: '',
            listName: 'group',
            data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
            key: "id",
            value: "name",
            placeholderEnable: true,
            required:true,

        },{
            label: "Product",
            type: "select",
            placeholder: "Please Enter Product",
            model: "group_id",
            url: '/inventory/api/v1/products/',
            listName: 'product',
            key: "id",
            value: "name_eng",
            placeholderEnable: true,
            required:true,
            edit: true,

        },{
            label: "Product",
            type: "select",
            placeholder: "Please Enter Product",
            model: "group_id",
            url: '/inventory/api/v1/products/',
            listName: 'product',
            key: "id",
            value: "name_eng",
            placeholderEnable: true,
        },{
            label: "user",
            type: "select",
            multiple: true,
            placeholder: "Please Enter User",
            model: "group_id",
            url: '/auth/api/v1/users/',
            listName: 'user',
            key: "id",
            value: "email",
            placeholderEnable: true,
        },
        {
            label: "Search test",
            type: "search",
            placeholder: "Please Enter Searched text",
            model: "group",
            url: '/static/countrydata.json',
            // data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
            key: "code",
            value: "name",
            required:true,

            // placeholderEnable: true,
            limit:4,
        },{
            label: "Search",
            type: "search",
            placeholder: "Please Enter Searched text",
            model: "group2",
            url: '/inventory/api/v1/products/',
            // data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
            key: "id",
            value: "name_eng",
            resultTemplateUrl: '/static/apps/ngCrud/ngCrud/templates/html/search_result.html',
            // placeholderEnable: true,
            limit:4,
            multiple: true,
            // onSelect: function(item, model){
            //     debugger
            //     model = "ahmed";
            //     return model;
            // },
            delay:1000,
        },
        ]
    };


    angular.bootstrap(document.body, ['test']);
});
