define([], function(){
    /**
    * authServ Module
    *
    * Description
    */
    angular.module('authServ', ['authProviders', 'ngCookies']).factory('authServ', ['$http', '$cookies', 'authSettings', authentication])
    function authentication ($http, $cookies, authSettings) {
        var out= {};
        out.authentication = function(user){
            return $http.post(authSettings.loginUrl, user);
        };

        out.checkAuth = function () {
            return $http.post(authSettings.checkUrl).then(success, error);
        }


        out.logout = function () {
            return $http.post(authSettings.logoutUrl).then(success, error);
        }

        function success (responce) {
            return responce.data;
        }

        function error (responce) {
            return responce.data;
        }
        return out;
    }
});

