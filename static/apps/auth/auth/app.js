define(['./controller/auth_ctrl','./services/auth_serv','./provider/auth_provider','./directives/auth_dir'],function(){
/**
* authApp Module
*
* Description
*/
angular.module('authApp', ['authCtrl', 'authServ', 'authProviders', 'authDir'])
.run(['authSettings', function (authSettings) {
    // debugger
}]);
});
