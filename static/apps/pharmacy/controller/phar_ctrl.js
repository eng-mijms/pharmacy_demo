/* global angular, define */
define([], function () {
      /**
      * authCtrl Module
      *
      * Description
      */
      angular.module('pharCtrl', ['ui.router']).controller('headerCtrl', ['$stateParams', 'pharSettings', headerCtrl])
        .controller('SideCtrl', ['$stateParams', 'pharSettings', '$state', SideCtrl])

      function SideCtrl ($stateParams, pharSettings, $state) {
          var side = this
          side.settings = $stateParams.sidebarSettings || pharSettings.sidebarSettings
          side.show = []
          angular.forEach(side.settings, function (d) {
            if (d.dropDown) {
              d.dropDown.forEach(function (val) {
                if ($state.current.name === val.state) {
                  // debugger
                  side.show[d.name] = true
                }
              })
            }
          })
        }
      // debugger

      function headerCtrl ($stateParams, pharSettings) {
        // debugger
        var head = this
        head.settings = $stateParams.headSettings || pharSettings.headSetting
        // debugger
        // us.authenticate = function () {
        //     authServ.authentication(us.user).then(function (data){
        //         console.log('Authentication Success')
        //         $state.go(authSettings.mainState)
        //     }, function (data) {
        //         console.log('authentication error')
        //     })
        // }

        // us.logout = function () {
        //     authServ.logout().then(function (data){
        //         $state.go(authSettings.loginState)
        //     }, function (data){
        //         console.log('logout Error')
        //     })
        // }
      }
    })
