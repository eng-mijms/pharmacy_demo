/* global define, angular */
define([], function () {
  /**
   * authProviders Module
   *
   * Description
   */
  angular.module('pharProvider', []).provider('pharSettings', [pharSettings])

  function pharSettings () {
    // var name = this.name
    // root path
    this.rootUrl = '/static/apps/pharmacy/'
    this.name = 'pharmacy_'
    this.$stateProviderRef
    // templates pathes
    this.headerTemplateUrl = this.rootUrl + 'template/html/header.html'
    this.footerTemplateUrl = this.rootUrl + 'template/html/footer.html'
    this.sideBarTemplateUrl = this.rootUrl + 'template/html/sidebar.html'
    this.signinTemplateUrl = '/static/apps/auth/auth/template/html/signin.html'

    // setting variables
    this.mainLocationUrl = '/'
    this.headSettings = {
      title: 'E-Pharmacy',
      urls: [{
        name: 'Home',
        state: this.name + 'home'
      }]
    }
    this.sidebarSettings = [
      {
        text: 'Home',
        state: this.name + 'home',
        icon: 'fa-dashboard'
      }, {
        text: 'Pharmacy',
        name: 'pharmacy',
        icon: 'fa-archive',
        dropDown: [
          {
            text: 'Drugs',
            state: this.name + 'product',
            icon: 'fa-hourglass-start'
          }, {
            text: 'Drug Items',
            state: this.name + 'item',
            icon: 'fa-bars'
          }, {
            text: 'Store Locations',
            state: this.name + 'location',
            icon: 'fa-shopping-cart'
          }, {
            text: 'Doctors',
            state: this.name + 'doctor',
            icon: 'fa-user-md'
          }, {
            text: 'Measurement Units',
            state: this.name + 'unit',
            icon: 'fa-pencil'
          }, {
            text: 'Drugs Materials',
            state: this.name + 'material',
            icon: 'fa-home'
          }, {
            text: 'Categories',
            state: this.name + 'category',
            icon: 'fa-tags'
          }, {
            text: 'Suppliers',
            state: this.name + 'supplier',
            icon: 'fa-building-o'
          }, {
            text: 'Customers',
            state: this.name + 'customer',
            icon: 'fa-user'
          }, {
            text: 'Fab Companies',
            state: this.name + 'fabcompany',
            icon: 'fa-industry'
          }, {
            text: 'Bar Codes',
            state: this.name + 'barcode',
            icon: 'fa-barcode'
          }, {
            text: 'Phamacies',
            state: this.name + 'warehouse',
            icon: 'fa-cloud'
          }
        ]

      }, {
        text: 'Orders',
        name: 'order',
        icon: 'fa-file-text-o',
        dropDown: [
          {
            text: 'Bills',
            state: this.name + 'bills',
            icon: 'fa-file-text'
          }, {
            text: 'Transactions',
            state: this.name + 'transaction',
            icon: 'fa-file-text-o'
          }, {
            text: 'Unit Convert',
            state: this.name + 'unit_conversion',
            icon: 'fa-pencil'
          }, {
            text: 'Payment Method',
            state: this.name + 'payment',
            icon: 'fa-cc-visa'
          }
        ]
      }, {
        text: 'Accounts',
        name: 'user',
        icon: 'fa-user',
        dropDown: [
          {
            text: 'Account',
            state: this.name + 'user',
            icon: 'fa-user'
          }, {
            text: 'Groups',
            state: this.name + 'group',
            icon: 'fa-users'
          }, {
            text: 'Group Permission',
            state: this.name + 'group_permission',
            icon: 'fa-pencil'
          }
        ]

      }
    ]
    // this.sidebarSettings = [{name: 'Home', url: this.name + 'test'}]
    this.title = 'Pharmacy '
    // this.ngCrudPath =
    this.$get = [function () {
      return {
        rootUrl: this.rootUrl,
        name: this.name,
        headerTemplateUrl: this.headerTemplateUrl,
        footerTemplateUrl: this.footerTemplateUrl,
        $stateProviderRef: this.$stateProviderRef,
        sideBarTemplateUrl: this.sideBarTemplateUrl,
        signinTemplateUrl: this.signinTemplateUrl,
        mainLocationUrl: this.mainLocationUrl,
        headSetting: this.headSettings,
        title: this.title,
        sidebarSettings: this.sidebarSettings
      }
    }]
  }
})
