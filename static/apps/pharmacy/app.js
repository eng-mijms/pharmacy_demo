/* global define, angular */
define(['../ngCrud/ngCrud/app', '../auth/auth/app', './provider/pharmacy_provider', './controller/phar_ctrl'], function () {
  /**
   * test Module
   *
   * Description
   */

  angular.module('pharmacy', ['ngCrud', 'authApp', 'pharProvider', 'pharCtrl'])
      .config(['$locationProvider', '$stateProvider', '$resourceProvider', 'crudSettingsProvider', 'authSettingsProvider', 'pharSettingsProvider',
            function ($locationProvider, $stateProvider, $resourceProvider, crudSettingsProvider, authSettingsProvider, pharSettingsProvider) {
              //  debugger
              //  authSettingsProvider.mainState = "test"
              pharSettingsProvider.$stateProviderRef = $stateProvider
              //  $resourceProvider.defaults.stripTrailingSlashes = false
              //  crudSettingsProvider.setRootUrl('/static/')
              //  debugger
              $locationProvider.html5Mode(true)
            }
        ])
    .run(['$state', '$http', '$rootScope', 'authServ', 'authSettings', 'pharSettings', 'crudSettings',
        function ($state, $http, $rootScope, authServ, authSettings, pharSettings, crudSettings) {
          var main = crudSettings.mainUrl
          var headerTemplateUrl = pharSettings.headerTemplateUrl
          var name = pharSettings.name
          var url = pharSettings.mainLocationUrl
          var signinTemplateUrl = pharSettings.signinTemplateUrl
          var title = pharSettings.title
          var sidebarTemplateUrl = pharSettings.sideBarTemplateUrl
          // debugger
          //  console.log(name+'home')
          pharSettings.$stateProviderRef.state(name + 'home', {
            url: url || '/',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }

            },
            title: title + 'Home',
            params: {
              settings: setting
            }
          }).state(name + 'location', {
            url: url + '/location',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Location',
            params: {
              settings: locationSettings
            }
          }).state(name + 'unit', {
            url: url + '/unit',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Unit',
            params: {
              settings: unitSettings
            }
          }).state(name + 'material', {
            url: url + '/material',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Material',
            params: {
              settings: materialSettings
            }
          }).state(name + 'category', {
            url: url + '/category',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Category',
            params: {
              settings: catSettings
            }
          }).state(name + 'supplier', {
            url: url + '/supplier',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Supplier',
            params: {
              settings: supplierSettings
            }
          }).state(name + 'customer', {
            url: url + '/customer',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Customer',
            params: {
              settings: customerSettings
            }
          }).state(name + 'fabcompany', {
            url: url + '/fabcompany',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Category',
            params: {
              settings: fabCompanySettings
            }
          }).state(name + 'product', {
            url: url + '/product',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Product',
            params: {
              settings: productSettings
            }
          }).state(name + 'login', {
            url: url + '/login',
            title: title + 'login',
            views: {
              'content': {
                controller: 'userCtrl as us',
                templateUrl: signinTemplateUrl
              }
            }
          }).state(name + 'item', {
            url: url + '/item',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Item',
            params: {
              settings: itemSettings
            }
          }).state(name + 'payment', {
            url: url + '/payment',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Payment',
            params: {
              settings: paymentSettings
            }
          }).state(name + 'barcode', {
            url: url + '/barcode',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Barcode',
            params: {
              settings: barcodeSettings
            }
          }).state(name + 'doctor', {
            url: url + '/doctor',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Doctor',
            params: {
              settings: doctorSettings
            }
          }).state(name + 'unit_conversion', {
            url: url + '/unit/conversion',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Conversions',
            params: {
              settings: unitconversionSettings
            }
          }).state(name + 'bills', {
            url: url + '/bill',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Bills',
            params: {
              settings: billSettings
            }
          }).state(name + 'warehouse', {
            url: url + '/warehouse',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Ware House',
            params: {
              settings: warehouseSettings
            }
          }).state(name + 'transaction', {
            url: url + '/transaction',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Transactions',
            params: {
              settings: tranSettings
            }
          }).state(name + 'group', {
            url: url + '/group',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Group',
            params: {
              settings: groupSettings
            }
          }).state(name + 'group_permission', {
            url: url + '/group/permission',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Group Permission',
            params: {
              settings: groupPermissionSettings
            }
          }).state(name + 'user', {
            url: url + '/user',
            views: {
              'header': {
                controller: 'headerCtrl as head',
                templateUrl: headerTemplateUrl
              },
              'sidebar': {
                // debugger
                controller: 'SideCtrl as side',
                templateUrl: sidebarTemplateUrl
              },
              'content': {
                controller: 'CrudCtrl as cu',
                templateUrl: main
              }
            },
            title: title + 'Group Permission',
            params: {
              settings: userSettings
            }
          })

          $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            //  debugger
            if (!$rootScope.User && toState.name !== authSettings.loginState) {
              event.preventDefault()
            }
            $rootScope.pageTitle = toState.title
            authServ.checkAuth().then(function (data) {
              if (data.user) {
                $rootScope.User = data.user
                if (toState.name === authSettings.loginState) {
                  $state.go(authSettings.mainState)
                } else {
                  $state.go(toState.name)
                }
              } else {
                $state.go(authSettings.loginState)
              }
            }, function (data) {
              if (!toState.allAccess) {
                $state.go(authSettings.loginState)
              }
            })
          })
        }
    ])

  var inventoryUrl = '/inventory/api/v1/'
  var itemTemplateResult = '/static/apps/pharmacy/template/html/item-search.html'
  var permission = {
    perModel: '',
    getPerModel: function (model, User) {
        var out = {}
        try {
          User.group.permissions.forEach(function (permission) {
            if (permission.modelname === model) {
              // debugger
              out = permission
              return
            }
          })
        } catch (e) { }
        return out
      },
    get: function (model, User) {
      try {
        // console.log(User.group.permissions);
        // console.log(this.getPerModel(model, User));
        // var temp = this.getPerModel(model, User)
        // debugger
        return User.is_admin || this.getPerModel(model, User).get
      }catch (e) {}
      return null
    },
    update: function (model, User) {
      try {
        return User.is_admin || this.getPerModel(model, User).edit
      }catch (e) {}
      return null
    },
    create: function (model, User) {
      try {
        return User.is_admin || this.getPerModel(model, User).add
      }catch (e) {}
      return null
    },
    delete: function (model, User) {
      try {
        return User.is_admin || this.getPerModel(model, User).delete
      }catch (e) {}
      return null
    }
  }
  //     popup settings
  var popUps = {
    materials: [
      {
        label: 'Fabricate Company',
        type: 'select',
        url: inventoryUrl + 'materials/',
        listName: 'material',
        //  key:'id',
        value: 'name',
        placeholder: 'Please Select Material',
        model: 'material',
        required: true,
        edit: true
      }, {
        label: 'Consentration',
        type: 'number',
        model: 'conc',
        placeholder: 'Please Enter Material Consentration',
        required: true,
        edit: true
      }, {
        label: 'Concentration Unit',
        type: 'select',
        model: 'unit_id',
        listName: 'unit',
        url: inventoryUrl + 'units/',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Material Consentration Unit',
        placeholderEnable: true,
        required: true,
        edit: true
      }],
    items: [
      {
        label: 'Item',
        type: 'search',
        placeholder: 'Please Search with Item Name',
        url: inventoryUrl + 'search/item/',
        // key: 'id',
        value: 'product',
        resultTemplateUrl: itemTemplateResult,
        model: 'item',
        required: true,
        edit: true,
        onSelect: function (item, model, fields, cuitem, crudServ) {
          //  debugger
          // debugger
          // cuitem.unit_to_id = item.ref_unit.id
          var field = fields[3]
          var itemSearchUrl = '/inventory/api/v1/search/unit/conversion/product/'
          // debugger
          crudServ.getAbsluoteUrl(itemSearchUrl, {q: item.product.id}).then(function (responce) {
            // debugger
            field.disable = false
            field.placeholder = 'Please Select One Unit'
            crudServ.lists[field.listName] = responce.data
            // debugger
          }, function () {
            console.log('Error in Getting Unit Conversion for this product')
          })
          return model
          //  this
        },
        formatter: function (model, searchResult) {
          //  console.log(searchData)
          // debugger
          var out
          searchResult.forEach(function (value) {
            //  debugger
            if (value === model) {
              //  debugger
              out = value.product.name_eng
              return
            }
          })
          //  debugger
          return out
        }
      }, {
        label: 'Discount',
        type: 'number',
        model: 'discount',
        placeholder: 'Please Enter discount number',
        edit: true
      }, {
        label: 'Quantity',
        type: 'number',
        model: 'quantity',
        placeholder: 'Please Enter Quantity',
        required: true,
        edit: true
      }, {
        label: 'Unit',
        type: 'select',
        model: 'unit_id',
        listName: 'unitProductList',
        // url: inventoryUrl + 'units/',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select One Item First',
        disable: true,
        required: true,
        edit: true
      }, {
        label: 'Paid',
        type: 'number',
        model: 'paid',
        placeholder: 'Please Enter Paid Amount',
        edit: true
      }, {
        label: 'Comment',
        type: 'areatext',
        model: 'comment',
        placeholder: 'Please Enter Your Comment Here',
        edit: true
      }], itemsTrans: [
        {
          label: 'Item',
          type: 'search',
          placeholder: 'Please Search with Item Name',
          url: inventoryUrl + 'search/item/',
          // key: 'id',
          value: 'product',
          resultTemplateUrl: itemTemplateResult,
          model: 'item',
          required: true,
          edit: true,
          onSelect: function (item, model, fields, cuitem, crudServ) {
            //  debugger
            // debugger
            // cuitem.unit_to_id = item.ref_unit.id
            var field = fields[2]
            var itemSearchUrl = '/inventory/api/v1/search/unit/conversion/product/'
            // debugger
            crudServ.getAbsluoteUrl(itemSearchUrl, {q: item.product.id}).then(function (responce) {
              // debugger
              field.disable = false
              field.placeholder = 'Please Select One Unit'
              crudServ.lists[field.listName] = responce.data
              // debugger
            }, function () {
              console.log('Error in Getting Unit Conversion for this product')
            })
            return model
            //  this
          },
          formatter: function (model, searchResult) {
            //  console.log(searchData)
            // debugger
            var out
            searchResult.forEach(function (value) {
              //  debugger
              if (value === model) {
                //  debugger
                out = value.product.name_eng
                return
              }
            })
            //  debugger
            return out
          }
        }, {
          label: 'Quantity',
          type: 'number',
          model: 'quantity',
          placeholder: 'Please Enter Quantity',
          required: true,
          edit: true
        }, {
          label: 'Unit',
          type: 'select',
          model: 'unit_id',
          listName: 'unitProductList',
          // url: inventoryUrl + 'units/',
          key: 'id',
          value: 'name',
          placeholder: 'Please Select One Item First',
          disable: true,
          required: true,
          edit: true
        }, {
          label: 'Cost',
          type: 'number',
          model: 'cost',
          placeholder: 'Please Enter Cost Amount',
          edit: true
        }, {
          label: 'Comment',
          type: 'areatext',
          model: 'comment',
          placeholder: 'Please Enter Your Comment Here',
          edit: true
        }]

  }

   // var headSettings={title:'E-Pharmacy',urls:[{name:'Home',state:name+'home'},{name:'Test',state:name+'test'}]}
  var locationSettings = {
    title: 'Store Location',
    index: 'Num',
    url: inventoryUrl + 'locations/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Desc',
      view: 'desc'
    }, {
      head: 'Capacity',
      view: 'capacity'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Store Location Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Description',
      type: 'textarea',
      placeholder: 'Please Enter Store Location Description',
      model: 'desc',
      edit: true
    }, {
      label: 'Capacity',
      type: 'number',
      placeholder: 'Please Enter Location Capacity',
      model: 'capacity',
      edit: true
    }],
    permissions: {
      model: 'location',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // console.log(permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User))
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // catsettings
  var catSettings = {
    title: 'Category',
    index: 'Num',
    url: inventoryUrl + 'categories/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Desc',
      view: 'desc'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Category Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Description',
      type: 'textarea',
      placeholder: 'Please Enter Category Description',
      model: 'desc',
      edit: true
    }], permissions: {
      model: 'category',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // unitsettings
  var unitSettings = {
    title: 'Unit',
    index: 'Num',
    url: inventoryUrl + 'units/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Desc',
      view: 'desc'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Unit Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Description',
      type: 'textarea',
      placeholder: 'Please Enter Unit Description',
      model: 'desc',
      edit: true
    }], permissions: {
      model: 'unit',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // material settings
  var materialSettings = {
    title: 'Material',
    index: 'Num',
    url: inventoryUrl + 'materials/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Comment',
      view: 'comment'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Material Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Comment',
      type: 'textarea',
      placeholder: 'Please Enter Material comment',
      model: 'comment',
      edit: true
    }], permissions: {
      model: 'material',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // Payement settings
  var paymentSettings = {
    title: 'Payment',
    index: 'Num',
    url: inventoryUrl + 'payments/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Description',
      view: 'desc'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Payment Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Description',
      type: 'textarea',
      placeholder: 'Please Enter Payment Description',
      model: 'desc',
      edit: true
    }], permissions: {
      model: 'payment',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // Payement settings
  var doctorSettings = {
    title: 'Doctor',
    index: 'Num',
    url: inventoryUrl + 'doctors/:id/',
    views: [{
      head: 'First Name',
      view: 'firstname'
    }, {
      head: 'Last Name',
      view: 'lastname'
    }],
    fields: [{
      label: 'First Name',
      type: 'text',
      placeholder: 'Please Enter Doctor First Name',
      model: 'firstname',
      required: true,
      edit: true
    }, {
      label: 'Last Name',
      type: 'text',
      placeholder: 'Please Enter Doctor First Name',
      model: 'lastname',
      required: true,
      edit: true
    }], permissions: {
      model: 'doctor',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // Payement settings
  var unitconversionSettings = {
    title: 'Unit Conversions',
    index: 'Num',
    url: inventoryUrl + 'unit_conversions/:id/',
    views: [
      {
        head: 'Product',
        expression: function (item) {
          try {
            return item.product.name_eng
          } catch (e) {
            return
          }
        }
      }, {
        head: 'Unit From',
        expression: function (item) {
          return item.unit_from.name
        }
      }, {
        head: 'Unit To',
        expression: function (item) {
          return item.unit_to.name
        }
      }, {
        head: 'Factor',
        view: 'factor'
      }
    ],
    fields: [{
      label: 'Product',
      type: 'search',
      url: inventoryUrl + 'search/product/',
      key: 'id',
      value: 'name_eng',
      placeholder: 'Please Search For Product unit Name',
      model: 'product_id',
      //  required: true,
      // edit: false,
      limit: 10,
      onSelect: function (item, model, fields, cuitem) {
        //  debugger
        cuitem.unit_to_id = item.ref_unit.id
        fields[2].disable = true
        //  debugger
        return model
        //  this
      },
      extracts: [{
        text: 'Create New Product',
        class: 'btn-warning',
        permission: function (User) {
          return productSettings.permissions.create(User)
        },
        action: function (popupServ, crudServ) {
          popupServ.servActions.openPopup({}, productSettings.fields, 'Create New Product', true).result.then(
                        function (result) {
                          try {
                            crudServ.crudResource(productSettings.url).save(result,
                                function (result) {}
                            )
                          } catch (e) {}
                        },
                        function () {
                          console.log('Create new Product is canceled')
                        }
                    )
        }
      }]
    }, {
      label: 'Unit From',
      type: 'select',
      listName: 'unit',
      url: inventoryUrl + 'units/',
      key: 'id',
      value: 'name',
      placeholder: 'Please Select unit Name',
      model: 'unit_from_id',
      required: true,
      edit: true
    }, {
      label: 'Unit To',
      type: 'select',
      listName: 'unit',
      url: inventoryUrl + 'units/',
      key: 'id',
      value: 'name',
      placeholder: 'Please Select unit Name',
      model: 'unit_to_id',
      required: true,
      edit: true
      //  disable: true,
    }, {
      label: 'Factor',
      type: 'text',
      placeholder: 'Please Enter Conversion Factor',
      model: 'factor',
      required: true,
      edit: true
    }],
    // preSubmit: function (item) {
    //   //  item.test = "new"
    // },
    postSubmit: function (data, fields) {
      fields[2].disable = false
    }, permissions: {
      model: 'unit_conversions',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
    //  submit: function (item, list, crudServ, fields){
    //      debugger
    //  }
  }

  // Payement settings
  var warehouseSettings = {
    title: 'Phamacies',
    index: 'Num',
    url: inventoryUrl + 'warehouses/:id/',
    views: [{
      head: 'Contact',
      view: 'contact_person'
    }, {
      head: 'Name',
      view: 'name_eng'
    }, {
      head: 'Email',
      view: 'email'
    }],
    fields: [{
      label: 'English Name',
      type: 'text',
      placeholder: 'Please Enter Pharmacy English Name',
      model: 'name_eng',
      required: true,
      edit: true
    }, {
      label: 'Arabic Name',
      type: 'text',
      placeholder: 'Please Enter Pharmacy Arabic Name',
      model: 'name_ar',
      //  required: true,
      edit: true
    }, {
      label: 'Email',
      type: 'text',
      placeholder: 'Please Enter Pharmacy Email',
      model: 'email',
      required: true,
      edit: true
    }, {
      label: 'Address',
      type: 'text',
      placeholder: 'Please Enter Pharmacy Address',
      model: 'address',
      //  required: true,
      edit: true
    }, {
      label: 'Telephone',
      type: 'text',
      placeholder: 'Please Enter Pharmacy tel',
      model: 'tel',
      //  required: true,
      edit: true
    }, {
      label: 'Mobile',
      type: 'text',
      placeholder: 'Please Enter Pharmacy mobile',
      model: 'mobile',
      //  required: true,
      edit: true
    }, {
      label: 'Account ID',
      type: 'number',
      placeholder: 'Please Enter Account ID',
      model: 'acc_id',
      //  required: true,
      edit: true
    }], permissions: {
      model: 'warehouse',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // supplier settings
  var supplierSettings = {
    title: 'Supplier',
    index: 'Num',
    url: inventoryUrl + 'suppliers/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Tel',
      view: 'tel'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Supplier Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Telephone',
      type: 'text',
      placeholder: 'Please Enter Supplier Telephone',
      model: 'tel',
      edit: true
    }, {
      label: 'Email',
      type: 'email',
      placeholder: 'Please Enter Supplier Email',
      model: 'email',
      edit: true
    }, {
      label: 'Address',
      type: 'text',
      placeholder: 'Please Enter Supplier address',
      model: 'address',
      edit: true
    }, {
      label: 'Priority',
      type: 'number',
      placeholder: 'Please Enter Supplier Priority',
      model: 'priority',
      edit: true
    }, {
      label: 'Comment',
      type: 'textarea',
      placeholder: 'Please Enter Customer comment',
      model: 'desc',
      edit: true
    }], permissions: {
      model: 'supplier',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // customer settings
  var customerSettings = {
    title: 'Customer',
    index: 'Num',
    url: inventoryUrl + 'customers/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Tel',
      view: 'tel'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Customer Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Telephone',
      type: 'text',
      placeholder: 'Please Enter Custoemr Telephone',
      model: 'tel',
      edit: true
    }, {
      label: 'Address',
      type: 'text',
      placeholder: 'Please Enter Custoemr address',
      model: 'address',
      edit: true
    }, {
      label: 'Priority',
      type: 'number',
      placeholder: 'Please Enter Custoemr Priority',
      model: 'priority',
      edit: true
    }, {
      label: 'Email',
      type: 'email',
      placeholder: 'Please Enter Custoemr email',
      model: 'email',
      edit: true
    }, {
      label: 'Comment',
      type: 'textarea',
      placeholder: 'Please Enter Customer comment',
      model: 'desc',
      edit: true
    }], permissions: {
      model: 'customer',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // fabricated company settings
  var fabCompanySettings = {
    title: 'Fabricate Company',
    index: 'Num',
    url: inventoryUrl + 'fab_companies/:id/',
    views: [{
      head: 'Name',
      view: 'name'
    }, {
      head: 'Country',
      view: 'country'
    }, {
      head: 'Comment',
      view: 'comment'
    }],
    fields: [{
      label: 'Name',
      type: 'text',
      placeholder: 'Please Enter Company Name',
      model: 'name',
      required: true,
      edit: true
    }, {
      label: 'Country',
      type: 'search',
      url: '/static/data/country.json',
      key: 'name',
      value: 'name',
      placeholder: 'Please search in Countries',
      model: 'country',
      method: 'static',
      required: true,
      delay: 10,
      edit: true
    }, {
      label: 'Comment',
      type: 'textarea',
      placeholder: 'Please Enter your Comment here',
      model: 'comment',
      edit: true
    }], permissions: {
      model: 'fab_company',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // second level data
  // product settings
  var productSettings = {
    title: 'Product',
    //  index: 'Num',
    url: inventoryUrl + 'products/:id/',
    views: [
      {
        head: 'Reg Num',
        view: 'reg_num'
      }, {
        head: 'Name',
        view: 'name_eng'
      }, {
        head: 'Company',
        expression: function (item) {
          try {
            return item.fabcompany.name
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Category',
        expression: function (item) {
          try {
            return item.category.name
          } catch (e) {
            return null
          }
        }
      }
    ],
    fields: [
      {
        label: 'Register Number',
        type: 'text',
        placeholder: 'Please Enter Product Register Number here',
        model: 'reg_num',
        required: true,
        edit: true
      }, {
        label: 'English Name',
        type: 'text',
        placeholder: 'Please Enter Product Engulish Name',
        model: 'name_eng',
        required: true,
        edit: true
      }, {
        label: 'Arabic Name',
        type: 'text',
        placeholder: 'Please Enter Product Arabic Name',
        model: 'name_ar',
        required: true,
        edit: true
      }, {
        label: 'Category',
        type: 'select',
        url: inventoryUrl + 'categories/',
        listName: 'category',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select one Category',
        model: 'category_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Fabricate Company',
        type: 'select',
        url: inventoryUrl + 'fab_companies/',
        listName: 'fabcompany',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Fabricate Company',
        model: 'fabcompany_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Refrence Unit',
        type: 'select',
        url: inventoryUrl + 'units/',
        listName: 'unit',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Refrence Unit',
        model: 'ref_unit_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Price',
        type: 'number',
        placeholder: 'Please Enter Price here',
        model: 'price',
        edit: true
      }, {
        label: 'Price Unit',
        type: 'select',
        url: inventoryUrl + 'units/',
        listName: 'unit',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select price Unit',
        model: 'price_unit_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Materials',
        type: 'list',
        show: function (item) {
          //  debugger
          return item.material.name
        },
        multiple: true,
        options: [{
          class: 'btn-primary',
          text: 'add Material',
          permission: function (User) {
            return true
          },
          action: function (item, popupServ) {
          //   debugger
            popupServ.servActions.openPopup({}, popUps.materials, 'Add New Material', true).result.then(
                          function (result) {
                            //  debugger
                            try {
                              item.materials.push(result)
                            } catch (e) {
                              item.materials = []
                              item.materials.push(result)
                            }
                          },
                          function () {
                            console.log('add new item is canceled')
                          }
                      )
          }
        }, {
          class: 'btn-warning',
          text: 'create New Material',
          listName: 'material',
          permission: function (User) {
            return materialSettings.permissions.create(User)
          },
          action: function (item, popupServ, crudServ) {
          //   debugger
            var listName = this.listName
            //  console.log(this.listName)
            //  return
            popupServ.servActions.openPopup({}, materialSettings.fields, 'Create New Material', true).result.then(
                          function (result) {
                            try {
                              crudServ.crudResource(materialSettings.url).save(result,
                                      function (result) {
                                        try {
                                          // debugger
                                          crudServ.lists[listName].push(result)
                                        } catch (e) {}
                                      }
                                  )
                            } catch (e) {
                              item.materials = []
                              item.materials.push(result)
                            }
                          },
                          function () {
                            console.log('add new item is canceled')
                          }
                      )
          }
        }],
        url: inventoryUrl + 'materials/',
        listName: 'material',
        key: 'id',
        selectSize: 40,
        value: 'name',
        placeholder: 'Please Select Materials',
        model: 'materials',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Product Size',
        type: 'number',
        placeholder: 'Please Enter Product Size here',
        model: 'product_size',
        edit: true
      }, {
        label: 'Product Size Unit',
        type: 'select',
        url: inventoryUrl + 'units/',
        listName: 'unit',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select price Unit',
        model: 'product_size_unit_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Priority',
        type: 'number',
        placeholder: 'Please Enter Product priority here',
        model: 'priority',
        edit: true
      }, {
        label: 'Min Limit',
        type: 'number',
        placeholder: 'Please Enter Product Minimum Limit here',
        model: 'min_limit',
        edit: true
      }, {
        label: 'Max Limit',
        type: 'number',
        placeholder: 'Please Enter Product Maximum Limit here',
        model: 'max_limit',
        edit: true
      }, {
        label: 'Max Discount',
        type: 'number',
        placeholder: 'Please Enter Product Maximum Discount here',
        model: 'max_disc',
        edit: true
      }, {
        label: 'Description',
        type: 'textarea',
        placeholder: 'Please Enter your Description here',
        model: 'desc',
        edit: true
      }
    ], permissions: {
      model: 'product',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        // console.log(User);
        return permission.create(this.model, User)
      },
      all: function (User) {
        console.log(permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User))
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  var itemSettings = {
    title: 'Item',
    //  index: 'Num',
    url: inventoryUrl + 'items/:id/',
    views: [
      {
        head: 'Product',
        expression: function (item) {
          try {
            return item.product.name_eng
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Unit',
        expression: function (item) {
          try {
            return item.unit.name
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Quantity',
        view: 'quantity'
      }
    ],
    fields: [
      {
        label: 'Product',
        type: 'search',
        placeholder: 'Please Enter Product Name',
        model: 'product_id',
        url: inventoryUrl + 'search/product/',
        key: 'id',
        value: 'name_eng',
        required: true,
        edit: true,
        limit: 10,
        onSelect: function (item, model, fields, cuitem, crudServ) {
          //  debugger
          // debugger
          // cuitem.unit_to_id = item.ref_unit.id
          var field = fields[1]
          var itemSearchUrl = '/inventory/api/v1/search/unit/conversion/product/'
          // debugger
          crudServ.getAbsluoteUrl(itemSearchUrl, {q: item.id}).then(function (responce) {
            // debugger
            field.disable = false
            field.placeholder = 'Please Select One Unit'
            crudServ.lists[field.listName] = responce.data
            // debugger
          }, function () {
            console.log('Error in Getting Unit Conversion for this product')
          })
          return model
        }
      }, {
        label: 'Unit',
        type: 'select',
        // url: inventoryUrl + 'units/',
        listName: 'itemUnit',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Product First',
        model: 'unit_id',
        disable: true,
        required: true,
        edit: true
      }, {
        label: 'Expiration Data',
        type: 'date',
        placeholder: 'Please Select Expiration Date',
        model: 'expire_date',
        edit: true
      }, {
        label: 'Cost Price',
        type: 'number',
        placeholder: 'Please Enter Cost Price',
        model: 'cost_price',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Price',
        type: 'number',
        placeholder: 'Please Enter Price here',
        model: 'price',
        edit: true
      }, {
        label: 'Purchase Price',
        type: 'number',
        placeholder: 'Please Enter Purchase Price',
        model: 'puchase_price',
        edit: true
      }, {
        label: 'Sale Price',
        type: 'number',
        placeholder: 'Please Enter Sale Price',
        model: 'sale_price',
        edit: true
      }, {
        label: 'Main BarCode',
        type: 'text',
        placeholder: 'Please Enter Main BarCode here',
        model: 'main_barcode',
        edit: true
      }, {
        label: 'Item Code',
        type: 'text',
        placeholder: 'Please Enter Main BarCode here',
        model: 'item_code',
        edit: true
      }, {
        label: 'Supplier',
        type: 'select',
        url: inventoryUrl + 'suppliers/',
        listName: 'supplier',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Supplier',
        model: 'supplier_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Store location',
        type: 'select',
        url: inventoryUrl + 'locations/',
        listName: 'location',
        key: 'id',
        value: 'name',
        placeholder: 'Please Select Store location',
        model: 'location_id',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Comment',
        type: 'textarea',
        placeholder: 'Please Enter Comment Here',
        model: 'comment',
        edit: true,
        extracts: [{
          text: 'Create New Product',
          class: 'btn-warning',
          permission: function (User) {
            return productSettings.permissions.create(User)
          },
          action: function (popupServ, crudServ) {
            popupServ.servActions.openPopup({}, productSettings.fields, 'Create New Product', true).result.then(
                          function (result) {
                            try {
                              crudServ.crudResource(productSettings.url).save(result,
                                  function (result) {}
                              )
                            } catch (e) {}
                          },
                          function () {
                            console.log('Create new Product is canceled')
                          }
                      )
          }
        }]
      }
    ],
    postSubmit: function (responce, fields) {
      var field = fields[1]
      field.disable = true
      field.placeholder = 'please Select Product First'
    }, permissions: {
      model: 'item',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  var barcodeSettings = {
    title: 'Bar Code',
    url: inventoryUrl + 'barcodes/:id/',
    views: [{
      head: 'Item',
      expression: function (item) {
        try {
          return item.item.product.name_eng
        } catch (e) {
          return null
        }
      }
    }, {
      head: 'Quantity',
      expression: function (item) {
        try {
          return item.item.quantity
        } catch (e) {
          return null
        }
      }
    }, {
      head: 'Bar Code',
      view: 'barcode'
    }],
    fields: [{
      label: 'Item',
      type: 'search',
      placeholder: 'Please Search with Item Name',
      url: inventoryUrl + 'search/item/',
      key: 'id',
      value: 'product',
      resultTemplateUrl: itemTemplateResult,
      model: 'item_id',
      required: true,
      edit: true,
      formatter: function (model, searchResult) {
        //  console.log(searchData)
        var out
        searchResult.forEach(function (value) {
          //  debugger
          if (value.id === model) {
            //  debugger
            out = value.product.name_eng
            return
          }
        })
        //  debugger
        return out
      }
    }, {
      label: 'barcode',
      type: 'text',
      placeholder: 'Please Enter BarCode',
      model: 'barcode',
      required: true,
      edit: true
    }], permissions: {
      model: 'barcode',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }

  }
  // group Settings
  var changePasswordFields = [
    {
      label: 'Password',
      type: 'password',
      model: 'password',
      placeholder: 'Please Enter Password Here.',
      required: true
      // edit: true
    }, {
      label: 'Confirm Password',
      type: 'password',
      model: 'confirm_password',
      compareTo: 'password',
      placeholder: 'Please Confirm Password.',
      required: true
      // edit: true
    }
  ]
  var authUrl = '/auth/api/v1/'
  var modelsData = [
    {
      key: 'account',
      name: 'Users'
    }, {
      key: 'group',
      name: 'Group'
    }, {
      key: 'permission',
      name: 'Add Permissions'
    }, {
      key: 'location',
      name: 'Store Location'
    }, {
      key: 'category',
      name: 'Category'
    }, {
      key: 'supplier',
      name: 'Supplier'
    }, {
      key: 'customer',
      name: 'Customer'
    }, {
      key: 'product',
      name: 'Drugs'
    }, {
      key: 'item',
      name: 'Stoke Drug Items'
    }, {
      key: 'doctor',
      name: 'Doctor'
    }, {
      key: 'warehouse',
      name: 'Pharmacy'
    }, {
      key: 'barcode',
      name: 'Bar Code'
    }, {
      key: 'payment',
      name: 'Payment Methods'
    }, {
      key: 'bill',
      name: 'Bill'
    }, {
      key: 'transaction',
      name: 'Transaction'
    }, {
      key: 'unit_conversions',
      name: 'Unit Conversion'
    }
  ]
  var userSettings = {
    title: 'User',
    index: 'Num',
    url: authUrl + 'users/:id/',
    views: [
      {
        head: 'FullName',
        view: 'full_name'
      }, {
        head: 'User Name',
        view: 'username'
      }, {
        head: 'Email',
        view: 'email'
      }, {
        head: 'Group',
        expression: function (item) {
          try {
            return item.group.name
          } catch (e) {}
        }
      }
    ],
    fields: [
      {
        label: 'Group',
        type: 'select',
        url: authUrl + 'groups/',
        listName: 'group',
        key: 'id',
        value: 'name',
        model: 'group_id',
        placeholder: 'Please Select One Group.',
        required: true,
        edit: true
      }, {
        label: 'User Name',
        type: 'text',
        model: 'username',
        placeholder: 'Please Enter User Name.',
        required: true,
        edit: true
      }, {
        label: 'Password',
        type: 'password',
        model: 'password',
        placeholder: 'Please Enter Password Here.',
        required: true
        // edit: true
      }, {
        label: 'Confirm Password',
        type: 'password',
        model: 'confirm_password',
        compareTo: 'password',
        placeholder: 'Please Confirm Password.',
        required: true
        // edit: true
      }, {
        label: 'Email',
        type: 'email',
        model: 'email',
        placeholder: 'Please Enter User Email.',
        required: true,
        edit: true
      }, {
        label: 'First Name',
        type: 'text',
        model: 'firstname',
        placeholder: 'Please Enter First Name.',
        // required: true,
        edit: true
      }, {
        label: 'Last Name',
        type: 'text',
        model: 'lastname',
        placeholder: 'Please Enter Last Name.',
        // required: true,
        edit: true
      }
    ],
    options: [
      {
        text: 'Change Password',
        class: 'btn-warning',
        permission: function (User) {
          return userSettings.permissions.update(User)
        },
        action: function (item, popupServ, crudServ, list) {
          var index = list.indexOf(item)
          // debugger
          popupServ.servActions.openPopup(item, changePasswordFields, 'change User Password', true).result.then(function (selectedItem) {
            // debugger
            crudServ.crudResource(userSettings.url + 'set_password/').update({id: item.id}, selectedItem).$promise.then(
                        function (responce) {
                          list[index] = responce
                        }, function (responce) {
                          console.log('updating Error')
                        })
            // list[index] = selectedItem
          }, function () {
            console.log('Edit Modal dismissed at: ')
          })
        }
      }
    ],
    permissions: {
      model: 'user',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }

  }
  var groupPermissionSettings = {
    title: 'Group Permissions',
    index: 'Num',
    url: authUrl + 'permissions/:id/',
    views: [
      {
        head: 'Model',
        view: 'modelname'
      },
      {
        head: 'Group',
        expression: function (item) {
          return item.group.name
        }
      },
      {
        head: 'Permissions',
        expression: function (d) {
          var out = ''
          if (d.get) {
            out += 'Get '
          }
          if (d.add) {
            out += 'Create '
          }
          if (d.edit) {
            out += 'Update '
          }
          if (d.delete) {
            out += 'Delete'
          }

          return out || 'No Permission Added'
        }
      }
    ],
    fields: [
      {
        label: 'Group',
        type: 'select',
        url: authUrl + 'groups/',
        listName: 'group',
        key: 'id',
        value: 'name',
        model: 'group_id',
        placeholder: 'Please Select One Group.',
        required: true,
        edit: true
      }, {
        label: 'Model',
        type: 'select',
        data: modelsData,
        key: 'key',
        value: 'name',
        model: 'modelname',
        placeholder: 'Please Select One Group.',
        required: true,
        edit: true
      }, {
        label: 'Get',
        type: 'checkbox',
        model: 'get',
        edit: true
      }, {
        label: 'Add',
        type: 'checkbox',
        model: 'add',
        edit: true
      }, {
        label: 'Update',
        type: 'checkbox',
        model: 'edit',
        edit: true
      }, {
        label: 'Delete',
        type: 'checkbox',
        model: 'delete',
        edit: true,
        extracts: [{
          text: 'Create New Group',
          class: 'btn-warning',
          listName: 'group',
          permission: function (User) {
            return groupSettings.permissions.create(User)
          },
          action: function (popupServ, crudServ) {
            // debugger
            var listName = this.listName
            popupServ.servActions.openPopup({}, groupSettings.fields, 'Create New Group', true).result.then(
                          function (result) {
                            try {
                              crudServ.crudResource(groupSettings.url).save(result,
                                  function (result) {
                                    // debugger
                                    try {
                                      crudServ.lists[listName].push(result)
                                    } catch (e) {}
                                  }
                              )
                            } catch (e) {}
                          },
                          function () {
                            console.log('Create new Product is canceled')
                          }
                      )
          }
        }]
      }
    ],
    permissions: {
      model: 'permission',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  var groupSettings = {
    title: 'Group',
    index: 'Num',
    url: authUrl + 'groups/:id/',
    views: [
      {
        head: 'Name',
        view: 'name'
      }, {
        head: 'Desc',
        view: 'desc'
      }, {
        head: 'Permission Number',
        expression: function (item) {
          try {
            return item.permissions.length
          } catch (e) {
            return null
          }
        }
      }
    ],
    fields: [
      {
        label: 'Name',
        type: 'text',
        placeholder: 'Please Enter Group Name',
        model: 'name',
        required: true,
        edit: true
      }, {
        label: 'Description',
        type: 'textarea',
        placeholder: 'Please Enter Group Description',
        model: 'desc',
        edit: true
      }
    ],
    permissions: {
      model: 'group',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User) {
        return permission.update(this.model, User)
      },
      delete: function (User) {
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // bill Settings
  var billSettings = {
    title: 'Bill',
    url: inventoryUrl + 'bills/:id/',
    index: 'Num',
    views: [
      {
        head: 'Doctor',
        expression: function (item) {
          try {
            return item.doctor.firstname
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Items Number',
        expression: function (item) { return item.bill_ds.length }
      }, {
        head: 'Payment',
        expression: function (item) { return item.payment.name }
      }],
    fields: [
      {
        label: 'Doctor',
        type: 'select',
        url: inventoryUrl + 'doctors/',
        listName: 'doctors',
        key: 'id',
        value: 'firstname',
        model: 'doctor_id',
        placeholder: 'please Select Doctor',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Payment',
        type: 'select',
        url: inventoryUrl + 'payments/',
        listName: 'payments',
        key: 'id',
        value: 'name',
        model: 'payment_id',
        placeholder: 'please Select payment method',
        required: true,
        edit: true
      }, {
        label: 'Customer',
        type: 'select',
        url: inventoryUrl + 'customers/',
        listName: 'customers',
        key: 'id',
        value: 'name',
        model: 'customer_id',
        placeholder: 'please Select Customer',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Comment',
        type: 'textarea',
        model: 'comment',
        placeholder: 'Please Enter Comment Here',
        edit: true
      }, {
        label: 'Items',
        type: 'list',
        multiple: true,
        show: function (item) {
          // debugger
          return item.item.product.name_eng
        },
        options: [{
          class: 'btn-primary',
          text: 'add Item',
          permission: function (User) {
            return true
          },
          action: function (item, popupServ, fields) {
          //   debugger
            popupServ.servActions.openPopup({}, popUps.items, 'Add New Item', true).result.then(
                          function (result) {
                            //  debugger
                            var field = popUps.items[3]
                            field.disable = true
                            field.placeholder = 'Please Select Item First'
                            try {
                              item.bill_ds.push(result)
                            } catch (e) {
                              item.bill_ds = []
                              item.bill_ds.push(result)
                            }
                          },
                          function (fields) {
                            console.log('add new item is canceled')
                            // debugger
                            var field = fields[3]
                            field.disable = true
                            field.placeholder = 'Please Select Item First'
                          }
                      )
          }
        }, {
          class: 'btn-warning',
          text: 'create New Item',
          permission: function (User) {
            return itemSettings.permissions.create(User)
          },
          listName: 'items',
          action: function (item, popupServ, crudServ) {
          //   debugger
            var listName = this.listName
            //  console.log(this.listName)
            //  return
            popupServ.servActions.openPopup({}, materialSettings.fields, 'Create New Item', true).result.then(
                          function (result) {
                            try {
                              crudServ.crudResource(itemSettings.url).save(result,
                                      function (result) {
                                        try {
                                          // debugger
                                          crudServ.lists[listName].push(result)
                                        } catch (e) {}
                                      }
                                  )
                            } catch (e) {
                              console.log('Error Creating New Item')
                            }
                          },
                          function () {
                            console.log('add new item is canceled')
                          }
                      )
          }
        }],
        edit: true,
        model: 'bill_ds',
        required: true
      }],
    preSubmit: function (item) { item.saved = true },
    // postSubmit: function (item) {}
    extend: [
      {
        text: 'Save Draft',
        class: 'btn-primary',
        validate: true,
        action: function (item, crudServ, list, cu) {
          // debugger
          var url = billSettings.url
          crudServ.crudResource(url).save(item, function (responce) {
            item = {}
            list.push(responce)
            cu.modelActive = true
          }, function (responce) {
            console.error('error saving bill item')
          })
        }
      }
    ], options: [
      {
        text: 'Confirm',
        class: 'btn-success',
        permission: function (User) {
          return billSettings.permissions.update(User)
        },
        action: function (item, popupServ, crudServ, list) {
          var index = list.indexOf(item)
          // debugger
          popupServ.servActions.openConfirmPopUp(item, 'This Changes Will Never Change Later', 'Confirm Bill').result.then(function (selectedItem) {
            selectedItem.saved = true
            // debugger
            crudServ.crudResource(billSettings.url + 'confirm/'). update({id: item.id}, selectedItem).$promise.then(
                        function (responce) {
                          list[index] = responce
                        }, function (responce) {
                          console.log('Confirming Transaction Error')
                        })
            // list[index] = selectedItem
          }, function () {
            console.log('Confirm Modal Dismissed')
          })
        }
      }
    ], permissions: {
      model: 'bill',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User, item) {
        try {
          if (item.saved) {
            return false
          }
        } catch (e) { }
        return permission.update(this.model, User)
      },
      delete: function (User, item) {
        try {
          if (item.saved) {
            return false
          }
        } catch (e) { }

        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }

  // transactions settings
  var tranSettings = {
    title: 'Transactions',
    url: inventoryUrl + 'transactions/:id/',
    index: 'Num',
    views: [
      {
        head: 'Title',
        view: 'title'
      }, {
        head: 'Items Number',
        expression: function (item) { return item.transactions.length }
      }, {
        head: 'Sender',
        expression: function (item) {
          try {
            return item.sender.name_eng
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Reciver',
        expression: function (item) {
          try {
            return item.reciver.name_eng
          } catch (e) {
            return null
          }
        }
      }, {
        head: 'Operation',
        expression: function (item) {
          try {
            return item.operation.name_eng
          } catch (e) {
            return null
          }
        }
      }],
    fields: [
      {
        label: 'Title',
        type: 'text',
        model: 'title',
        placeholder: 'please Enter Transaction Title',
        edit: true
      }, {
        label: 'Sender',
        type: 'select',
        url: inventoryUrl + 'warehouses/',
        listName: 'warehouses',
        key: 'id',
        value: 'name_eng',
        model: 'sender_id',
        placeholder: 'please Select Sender',
        placeholderEnable: true,
        edit: true
      }, {
        label: 'Reciver',
        type: 'select',
        url: inventoryUrl + 'warehouses/',
        listName: 'warehouses',
        key: 'id',
        value: 'name_eng',
        model: 'reciver_id',
        placeholder: 'please Select Reciever',
        placeholderEnable: true,
        required: true,
        edit: true
      }, {
        label: 'Operation',
        type: 'select',
        url: inventoryUrl + 'operations/',
        listName: 'operations',
        key: 'id',
        value: 'name_eng',
        model: 'operation_id',
        placeholder: 'please Select Operation',
        required: true,
        edit: true
      }, {
        label: 'Comment',
        type: 'textarea',
        model: 'comment',
        placeholder: 'Please Enter Comment Here',
        edit: true
      }, {
        label: 'Items',
        type: 'list',
        multiple: true,
        show: function (item) {
          // debugger
          return item.item.product.name_eng
        },
        options: [{
          class: 'btn-primary',
          text: 'add Item',
          permission: function (User) {
            return true
          },
          action: function (item, popupServ) {
          //   debugger
            popupServ.servActions.openPopup({}, popUps.itemsTrans, 'Add New Item', true).result.then(
                          function (result, fields) {
                            // debugger
                            var field = popUps.itemsTrans[2]
                            field.disable = true
                            field.placeholder = 'Please Select Item First'
                            try {
                              item.transactions.push(result)
                            } catch (e) {
                              item.transactions = []
                              item.transactions.push(result)
                            }
                          },
                          function (fields) {
                            console.log('add new item is canceled')
                            var field = fields[2]
                            field.disable = true
                            field.placeholder = 'Please Select Item First'
                          }
                      )
          }
        }, {
          class: 'btn-warning',
          text: 'create New Item',
          listName: 'items',
          permission: function (User) {
            return itemSettings.permissions.create(User)
          },
          action: function (item, popupServ, crudServ) {
          //   debugger
            var listName = this.listName
            //  console.log(this.listName)
            //  return
            popupServ.servActions.openPopup({}, materialSettings.fields, 'Create New Item', true).result.then(
                          function (result) {
                            try {
                              crudServ.crudResource(itemSettings.url).save(result,
                                      function (result) {
                                        try {
                                          // debugger
                                          crudServ.lists[listName].push(result)
                                        } catch (e) {}
                                      }
                                  )
                            } catch (e) {
                              console.log('Error Creating New Item')
                            }
                          },
                          function () {
                            console.log('add new item is canceled')
                          }
                      )
          }
        }],
        edit: true,
        model: 'transactions',
        required: true
      }],
    preSubmit: function (item) { item.saved = true },
    // postSubmit: function (responce, fields) {
    //   fields
    // }
    extend: [
      {
        text: 'Save Draft',
        class: 'btn-primary',
        validate: true,
        action: function (item, crudServ, list, cu) {
          // debugger
          var url = tranSettings.url
          crudServ.crudResource(url).save(item, function (responce) {
            item = {}
            list.push(responce)
            cu.modelActive = true
            cu.item = {}
          }, function (responce) {
            console.error('error saving trans item')
          })
        }
      }
    ], options: [
      {
        text: 'Confirm',
        class: 'btn-success',
        permission: function (User) {
          return tranSettings.permissions.update(User)
        },
        action: function (item, popupServ, crudServ, list) {
          var index = list.indexOf(item)
          // debugger
          popupServ.servActions.openConfirmPopUp(item, 'This Changes Will Never Change Later', 'Confirm Transaction').result.then(function (selectedItem) {
            selectedItem.saved = true
            // debugger
            crudServ.crudResource(tranSettings.url + 'confirm/').update({id: item.id}, selectedItem).$promise.then(
                        function (responce) {
                          list[index] = responce
                        }, function (responce) {
                          console.log('Confirming Transaction Error')
                        })
            // list[index] = selectedItem
          }, function () {
            console.log('Confirm Modal Dismissed')
          })
        }
      }
    ], permissions: {
      model: 'transaction',
      get: function (User) {
        return permission.get(this.model, User)
      },
      update: function (User, item) {
        try {
          if (item.saved) {
            return false
          }
        } catch (e) { }
        return permission.update(this.model, User)
      },
      delete: function (User, item) {
        try {
          if (item.saved) {
            return false
          }
        } catch (e) { }
        return permission.delete(this.model, User)
      },
      create: function (User) {
        return permission.create(this.model, User)
      },
      all: function (User) {
        // debugger
        return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
      }
    }
  }
  // third level data settings
  // var billSettings = {
  //   title: 'Bill',
  //   url: inventoryUrl + 'bills/:id/',
  //   views: [{
  //     head: 'Item',
  //     expression: function (d) { return d.product.name_eng }
  //   }
  //   ]
  // }

  var setting = {
    title: 'User',
    index: 'Num',
    url: inventoryUrl + 'users/:id/',
    options: [{
      class: 'btn-warning',
      text: 'change password',
      action: function (item, popupServ) {
        // debugger
        console.log('test')
      }
    }],
    views: [{
      head: 'name',
      view: 'fullname'
    }, {
      head: 'ID',
      view: 'id'
    }, {
      head: 'Email',
      view: 'email'
    }, {
      head: 'ID+100',
      expression: function (item) {
        return 7
      }
    }],
    fields: [{
      label: 'User Name',
      type: 'text',
      placeholder: 'Please Enter your Prefered user name',
      model: 'username',
      required: true,
      edit: true

    }, {
      label: 'password',
      type: 'password',
      //  label: "User Name",
      placeholder: 'Please Enter your Password',
      model: 'password'
    }, {
      label: 'Description',
      type: 'textarea',
      row: 10,
      col: 9,
      placeholder: 'Please Enter User Description',
      model: 'desc',
      required: true
    }, {
      label: 'Group',
      type: 'select',
      placeholder: 'Please Enter User Group',
      model: 'group_id',
      url: '',
      listName: 'group',
      data: [{
        id: 1,
        name: 'test'
      }, {
        id: 2,
        name: 'test 2'
      }, {
        id: 3,
        name: 'test 3'
      }, {
        id: 4,
        name: 'test 4'
      }],
      key: 'id',
      value: 'name',
      placeholderEnable: true,
      required: true
    }, {
      label: 'Product',
      type: 'select',
      placeholder: 'Please Enter Product',
      model: 'group_id',
      url: inventoryUrl + 'users/',
      listName: 'product',
      key: 'id',
      value: 'name_eng',
      placeholderEnable: true,
      required: true,
      edit: true

    }, {
      label: 'Product',
      type: 'select',
      placeholder: 'Please Enter Product',
      model: 'group_id',
      url: inventoryUrl + 'users/',
      listName: 'product',
      key: 'id',
      value: 'name_eng',
      placeholderEnable: true
    }, {
      label: 'user',
      type: 'select',
      multiple: true,
      placeholder: 'Please Enter User',
      model: 'group_id',
      url: '/auth/api/v1/users/',
      listName: 'user',
      key: 'id',
      value: 'email',
      placeholderEnable: true
    }, {
      label: 'Search test',
      type: 'search',
      placeholder: 'Please Enter Searched text',
      model: 'group',
      url: '/static/countrydata.json',
      //  data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
      key: 'code',
      value: 'name',
      required: true,

      //  placeholderEnable: true,
      limit: 4
    }, {
      label: 'Search',
      type: 'search',
      placeholder: 'Please Enter Searched text',
      model: 'group2',
      url: inventoryUrl + 'search/item/',
      //  data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
      key: 'id',
      value: 'name_eng',
      resultTemplateUrl: '/static/apps/ngCrud/ngCrud/templates/html/search_result.html',
      //  placeholderEnable: true,
      limit: 4,
      multiple: true,
      //  onSelect: function (item, model){
      //      debugger
      //      model = "ahmed"
      //      return model
      //  },
      delay: 1000,
      extracts: [{
        text: 'Add new item',
        class: 'btn-primary',
        permission: function (User) {
          return true
        },
        action: function (popupServ) {
          popupServ.servActions.openPopup({}, {}, 'Add new Item', true).result.then(function (item) {
            console.log(item)
          }, function () {
            console.log('modal dismiss')
          })
        }
      }]
    }]
  }
})
