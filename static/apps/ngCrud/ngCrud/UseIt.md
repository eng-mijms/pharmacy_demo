#ngCrud
##ngCrud Use Documentation

##Introduction

###ngCrud
stands for AngularJS crud (create, read, update, delete) operations. This documentation demonstrate for developers how can use it.

##Dependencies
 - [Angular](https://angularjs.org/)
 - [Ui Router](https://github.com/angular-ui/ui-router)
 - [Ui Bootstrap](https://angular-ui.github.io/bootstrap/)
 - [Require Js](http://requirejs.org/)
 - [Infinite Scroll Modified](https://sroze.github.io/ngInfiniteScroll/)


###Libraries modification
From line 32 until scope.$on('$destroy')

```javascript
          windowBottom = $window[0].innerHeight + $window[0].pageYOffset;
          elementBottom = elem[0].scrollTop + elem[0].offsetHeight;
          remaining = elementBottom - windowBottom;
          shouldScroll = remaining <= $window[0].innerHeight * scrollDistance;
          if (shouldScroll && scrollEnabled) {
            if ($rootScope.$$phase) {
              return scope.$eval(attrs.infiniteScroll);
            } else {
              return scope.$apply(attrs.infiniteScroll);
            }
          } else if (shouldScroll) {
            return checkWhenEnabled = true;
          }
        };
        $window.on('scroll', handler);
```

##Installing
```
git clone https://gitlab.com/eng-mijms/ngCrudLibs.git
```

##Running
###loading library
create a loading js file to include ngCrudFolder inside it
```javascript
require(['./app'], function(){
//your code go Here



//bootstrap your angular application like that
//replacing ng-app="myApp"
//with
angular.bootstrap(document.body, ['myApp']); // ng-app="myApp"
});
```

in your index.html include this loading file
```html
<script type="text/javascript" data-main="/{yourApp}/loading.js" src="{path to}/require.min.js"></script>
```

##Provider Settings
You can set root Folder and customize main if you want it your design
how can you get things inside your customized main please refer to dev Documentation

####crudSettingsProvider Settings
```javascript
    angular.module('test', ['ui.router', 'ngCrud']).config(['$stateProvider','$resourceProvider', 'crudSettingsProvider', function ($stateProvider, $resourceProvider, crudSettingsProvider ) {
    //crudSettingsProvider is our provider can set two params
    //rootUrl this is default '/static/apps/ngCrud/'
    //main is the template main.html file position default is '/static/apps/ngCrud/templates/html/main.html'
        $resourceProvider.defaults.stripTrailingSlashes = false;
        var main = crudSettingsProvider.mainUrl;

        $stateProvider.state('home',{
            url:'',
            controller: 'CrudCtrl as cu',
            templateUrl: main,
            params:{settings:setting}
        })
    }]);
```
#####Params
######rootUrl
is ngCrud url default is '/static/apps/ngCrud/'

######mainUrl
is ngCrud main template url default is '/static/apps/ngCrud/templates/html/main.html'
######params
```javascript
params:{settings:setting}
```
send params to library object please refere to library setting for more details
##Library Settings

```javascript
    var setting = {
        title:"User", //title of crud app (list User, create User)
        index:"Num", //show index (record Number) in list missing = no index
        url: '/inventory/api/v1/products/:id/', //deal with REST API url standard in backend
        views:[                 //list of objects to view in table

            //view object can contain elements (head, view, expression)
            {
                head:"name",        //head in table <th> </th>
                view:"fullname",    //point to retrived data object say user.fullname
            },{
                head:"ID",
                view:"id",
            },
            {
                head:"Email",
                view:"email",
            },
            {
                head:"ID+100",
                expression: function(item){ //expresion is callback function to execute
                    return 7;
                },
            }
        ],
        fields:[                //list of objects mapped to create form fields
        //one object can contain (label, type, placeholder, model, required, .....etc(coressponding to field type))
        {
            label: "User Name", // will show in label
            type: "text", //field type
            placeholder: "Please Enter your Prefered user name",//placeholder inside field
            model: "username",//ng-model to send to server
            required:true,//ng validation missing = false

        },
        {
            label: "password",
            type: "password",
            // label: "User Name",
            placeholder: "Please Enter your Password",
            model: "password",
        },
        {
            label: "Description",
            type: "textarea",
            row: 10,    // row for textarea default if not sended is 30
            col: 9,     // col for textarea default if not sended is 3
            placeholder: "Please Enter User Description",
            model: "desc",
            required:true,

        },
        {
            label: "Group",
            type: "select",
            placeholder: "Please Enter User Group",
            model: "group_id",
            listName: 'group',
            data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}], //data shown in select
            key: "id", //key selected in ng-model
            value: "name",  //name to shown to user
            placeholderEnable: true, //enable to chooce placeholder == undefined value
            required:true,

        },{
            label: "Product",
            type: "select",
            placeholder: "Please Enter Product",
            model: "group_id",
            url: '/inventory/api/v1/products/', //url to get list allow to pagnition under REST API standars
            listName: 'product',//stored list in memory for unrepeat data fetching
            key: "id",
            value: "name_eng",
            placeholderEnable: true,
            required:true,

        },{
            label: "Product",
            type: "select",
            placeholder: "Please Enter Product",
            model: "group_id",
            url: '/inventory/api/v1/products/',
            listName: 'product',
            key: "id",
            value: "name_eng",
            placeholderEnable: true,
        },{
            label: "user",
            type: "select",
            placeholder: "Please Enter User",
            model: "group_id",
            url: '/auth/api/v1/users/',
            listName: 'user',
            key: "id",
            value: "email",
            placeholderEnable: true,
        },
        {
            label: "Search test",
            type: "search",
            placeholder: "Please Enter Searched text",
            model: "group",
            url: '/static/countrydata.json',//search url default method get with q="searched string"
            key: "code",
            value: "name",
            required:true,
            limit:4, //limit viewed result default if not set 10
        },{
            label: "Search",
            type: "search",
            placeholder: "Please Enter Searched text",
            model: "group2",
            url: '/static/countrydata.json',
            // data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
            key: "name",
            value: "code",
            resultTemplateUrl: '/static/apps/ngCrud/templates/html/search_result.html', //customize result template to viewed
            limit:4,
            onSelect: function(item, model){ // call this function on search select
                debugger
                model = "ahmed";
                return model;
            },
            delay:1000,//delay when user type to send request to url 3000 millisecond
        },
        ]
    };
```
