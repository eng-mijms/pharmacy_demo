/* global angular, define*/
define(['../providers/crud_provider'], function () {
  /**
   * crudServ Module
   *
   * Description
   */
  angular.module('ngCrudServ', ['ngResource'])
  .factory('defaultServ', ['$http', defaultServ])
  .factory('crudServ', ['$http', '$resource', 'crudSettings', 'defaultServ', crudServ])
  .factory('EditPopupServ', ['$modal', 'crudSettings', 'crudServ', EditPopupServ])

  function defaultServ ($http) {
    function getAbsluoteUrl (url, pData) {
      // body...
      return $http.get(url, {params: pData}).then(function (data, status, headers, config) {
        return data.data
      }, function (data, status, headers, config) {
        return data.data
      })
    }

    function postAbsluoteUrl (url, data) {
      // body...
      return $http.post(url, data).then(function (data, status, headers, config) {
        return data.data
      }, function (data, status, headers, config) {
        return data.data
      })
    }

    function customPost (url, method, params, jsonData) {
      // body...
      return $http({
        method: method,
        url: url,
        params: params,
        data: jsonData
      })
    }

    return {
      getAbsluoteUrl: getAbsluoteUrl,
      postAbsluoteUrl: postAbsluoteUrl,
      customPost: customPost
    }
  }

  function crudServ ($http, $resource, crudSettings, defaultServ) {
    var lists = {}
    var loadings = {}

    function getAbsluoteUrl (url, pData) {
      // body...
      return $http.get(url, {params: pData})
    }

    function crudResource (url) {
      return $resource(url, {}, {
        query: {
          method: 'GET',
          isArray: false
        },
        'update': {
          method: 'PUT'
        }
      })
    }

    function getSelectedData (data, listName, url) {
      // debugger
      if (data) {
        return data
      } else if (url || listName) {
        if (lists[listName]) {
          return lists[listName]
        } else if (url) {
          lists[listName] = []
          getAllData(url, listName)
          // debugger
          return lists[listName]
        }
      } else {
        return []
      }
    }

    function getAllData (url, listName) {
      defaultServ.getAbsluoteUrl(url).then(
                function (data) {
                  lists[listName] = lists[listName].concat(data.results)
                  if (data.next) {
                    getAllData(data.next, listName)
                  }
                })
    }

    function objectFromString (item, string) {
      // debugger
      if (!item) {
        return
      }
      var o = angular.copy(item)
      var temp = string.split('.')
      for (var i = 0, n = temp.length ;i < n ;i++) {
        var k = temp[i]
        try {
          o = o[k]
        } catch (e) {
          return null
        }
      }
      return o
    }

    return {
      crudResource: crudResource,
      objectFromString: objectFromString,
      lists: lists,
      loadings: loadings,
      getSelectedData: getSelectedData,
      getAbsluoteUrl: getAbsluoteUrl

    }
  }

  function EditPopupServ ($modal, crudSettings, crudServ) {
    // debugger
    var servActions = {}
    // var popupServ = EditPopupServ
    // debugger
    //  servActions.executedFunc = {}
    servActions.openPopup = function (item, settings, title, type) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: crudSettings.rootUrl + 'templates/html/edit.html',
        controller: function ($scope, $modalInstance, items, fieldSettings, popupServ) {
          // debugger
          $scope.popupServ = popupServ
          $scope.crudServ = crudServ
          $scope.getSelectedData = function (data, listName, url) {
            return crudServ.getSelectedData(data, listName, url)
          }

          $scope.watchModelValid = function (field, form) {
            // debugger
            $scope.$watch('item.' + field.model + '.length', function (viewValue) {
              // debugger
              try {
                if (!viewValue) {
                  form[field.label].$setValidity('required', false)
                }else {
                  form[field.label].$setValidity('required', true)
                }
              } catch (e) {
                form[field.label].$setValidity('required', false)
              }
            })
          }

          $scope.fieldSettings = fieldSettings
          $scope.type = type
          $scope.title = title || 'Edit Item'
          $scope.item = angular.copy(items)
          // debugger
          $scope.ok = function () {
            // debugger
            $modalInstance.close($scope.item)
          }
          $scope.cancel = function () {
            $modalInstance.dismiss($scope.fieldSettings)
          }
        },
        resolve: {
          fieldSettings: function () {
            return settings
          },
          items: function () {
            return item
          },
          popupServ: function () {
            return EditPopupServ($modal, crudSettings, crudServ)
          },
          type: function () {
            // debugger
            return type
          }
        },
        size: 'lg'
      })

      return modalInstance
    }

    servActions.openConfirmPopUp = function (item, msg, title) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: crudSettings.rootUrl + 'templates/html/confirm.html',
        controller: function ($scope, $modalInstance, items) {
          $scope.title = title || 'Edit Item'
          $scope.msg = msg
          $scope.item = angular.copy(items)
          $scope.ok = function () {
            $modalInstance.close($scope.item, $scope.fieldSettings)
          }
          $scope.cancel = function () {
            $modalInstance.dismiss($scope.fieldSettings)
          }
        },
        resolve: {
          items: function () {
            return item
          }
        },
        size: 'lg'
      })

      return modalInstance
    }
    return {
      servActions: servActions
    }
  }
})
