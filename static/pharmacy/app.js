
  // var script = document.createElement('script');
  // script.type = 'text/javascript';
  // script.src = '/static/pharmacy/test.js'; 
  // var head = document.getElementsByTagName('head')[0];
  // head.appendChild(script)
  require.config({

  });
  
  define(['./loading'],function(){
      var app = angular.module('pharmacy2', ['ui.router','test']).controller('TestCtrl', ['$scope','testing', function ($scope,testing) {
        console.log('Test');
        console.log(testing.test);
      }])
      .config(['$stateProvider','$locationProvider',
        function ($stateProvider,$locationProvider) {
          $stateProvider.state('home',{
            url: "/",
            templateUrl: "static/app/templates/html/test.html",
            controller: "TestCtrl",
            title:"Item",
            // params:{settings:itemsettings}
          })
          
          $locationProvider.html5Mode(true);
          // debugger;
        }]);
      return app
  });
  
  

