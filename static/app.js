require(['./apps/pharmacy/app'], function () {
    /**
    * mainApp Module
    *
    * Description
    */
    angular.module('mainApp', ['pharmacy'])
    .config(['pharSettingsProvider', 'authSettingsProvider', '$stateProvider', '$locationProvider', '$resourceProvider',
     function (pharSettingsProvider, authSettingsProvider,$stateProvider, $locationProvider, $resourceProvider) {
        // debugger

        $resourceProvider.defaults.stripTrailingSlashes = false;
        $locationProvider.html5Mode(true);
        authSettingsProvider.mainState = 'pharmacy_home';
        authSettingsProvider.loginState = 'pharmacy_login';
        pharSettingsProvider.mainLocationUrl = '/pharmacy'
    }]).run(['$http', function ($http) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }]);

    angular.bootstrap(document, ['mainApp']);


})
