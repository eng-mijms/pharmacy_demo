# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0004_auto_20150911_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouppermission',
            name='group',
            field=models.ForeignKey(related_name='permissions', default=1, to='authentication.Group'),
            preserve_default=False,
        ),
    ]
