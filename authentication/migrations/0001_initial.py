# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.CharField(unique=True, max_length=40)),
                ('email', models.EmailField(max_length=254, unique=True, null=True, blank=True)),
                ('firstname', models.CharField(max_length=40, null=True, blank=True)),
                ('lastname', models.CharField(max_length=40, null=True, blank=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='GroupPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modelname', models.CharField(max_length=40)),
                ('add', models.BooleanField()),
                ('edit', models.BooleanField()),
                ('get', models.BooleanField()),
                ('delete', models.BooleanField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('group', models.ForeignKey(related_name='permissions', to='authentication.Group', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='account',
            name='group',
            field=models.ForeignKey(related_name='users', blank=True, to='authentication.Group', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='grouppermission',
            unique_together=set([('group', 'modelname')]),
        ),
    ]
