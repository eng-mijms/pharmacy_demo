# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0005_auto_20150911_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouppermission',
            name='add',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='grouppermission',
            name='delete',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='grouppermission',
            name='edit',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='grouppermission',
            name='get',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterUniqueTogether(
            name='grouppermission',
            unique_together=set([]),
        ),
    ]
