# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0003_group_desc'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouppermission',
            name='group',
            field=models.ForeignKey(related_name='permissions', blank=True, to='authentication.Group', null=True),
        ),
    ]
