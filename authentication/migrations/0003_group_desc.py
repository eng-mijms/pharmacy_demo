# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0002_account_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='desc',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
