"""Views Django."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import permissions, viewsets, status, views
from authentication.models import Account, Group, GroupPermission
from authentication.serializers import *
import json
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from authentication.permissions import *
from rest_framework.decorators import detail_route

class GroupPermissionsViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = GroupPermission.objects.all()
    serializer_class = GroupPermissionsSerializer
    permission_classes = (permissions.IsAuthenticated, PermissionPermissions,)


class AccountViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    # lookup_field = 'username'
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    # permission_classes = (permissions.IsAuthenticated,IsAccountOwner)
    permission_classes = (permissions.IsAuthenticated, AccountPermissions, )
    # def get_permissions(self):
    #   if self.request.method in permissions.SAFE_METHODS:
    #       return (permissions.AllowAny(),)

    #   if self.request.method == 'POST':
    #       return (permissions.AllowAny(),)
    #   # print "authentication"
    #   # print IsAccountOwner()
    #   return (permissions.IsAuthenticated(), IsAccountOwner(),)
    @detail_route(methods=['post', 'put'])
    def set_password(self, request, pk=None):
        user = self.get_object()
        serializer = PasswordSerializer(data=request.data)
        # print "password request"
        # print serializer.is_valid()
        print request.data
        if serializer.is_valid():
            # print serializer
            if request.data[u'password'] == request.data[u'confirm_password']:
                print "new Pass Settings"
                user.set_password(request.data[u'password'])
                user.save()
                serialize = AccountSerializer(user, context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):
        """Override Create."""
        serializer = self.serializer_class(data=request.data)
        # serializer.save(group=request.data['group'])
        # return Response(serializer, status=status.HTTP_201_CREATED)
        # print (request.data)
        # print serializer.is_valid()
        if serializer.is_valid():
            account = Account.objects.create_user(**serializer.validated_data)
            # serializer.save(group=request.data['group'])
            serialize = AccountSerializer(account, context={'request': request})
            return Response(serialize.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsAuthenticated, GroupPermissions,)


class LogInView(views.APIView):

    """Model Serializer View."""

    def post(self, request, format=None):
        """Login Post."""
        # print 'login'
        # print 'login view'
        data = json.loads(request.body)
        email = data.get('email', None)
        password = data.get('password', None)
        account = authenticate(email=email, password=password)
        # print account
        if account is not None:
            if account.is_active:
                login(request, account)
                serialized = AccountSerializer(
                    account, context={'request': request})
                return Response(serialized.data)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'this account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'password and email compinations is invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)


class LogOutView(views.APIView):

    """Model Serializer View."""

    # print 'logout'
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        """Override Post."""
        logout(request)
        # print request
        return Response({}, status=status.HTTP_204_NO_CONTENT)


class getUserView(views.APIView):

    """Model Serializer View."""

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        """Override Post Mehod."""
        serialized = AccountSerializer(
            request.user, context={'request': request})
        return Response({'user': serialized.data, })
