# -*- coding: utf-8 -*-
from __future__ import unicode_literals
"""angular1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from rest_framework import routers
# from django.contrib import admin
from authentication.views import *


router = routers.DefaultRouter()
router.register(r'groups', GroupViewSet)
router.register(r'users', AccountViewSet)
router.register(r'permissions', GroupPermissionsViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^logout/', LogOutView.as_view(), name='logout'),
    url(r'^check/', getUserView.as_view(), name='check'),
    url(r'^auth/', LogInView.as_view(), name='login'),
]
