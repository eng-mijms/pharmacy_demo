# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import update_session_auth_hash
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
# from rest_framework.renderers import JSONRenderer
from authentication.models import Account, Group, GroupPermission


class GroupPermissionsSerializers(serializers.ModelSerializer):
    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)
    class Meta:
        model = GroupPermission
        fields = ('modelname', 'add', 'edit', 'delete', 'get')
        read_only_fields = ('created_at', 'updated_at',)


class GroupSerializer(serializers.ModelSerializer):
    permissions = GroupPermissionsSerializers(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'permissions')
        read_only_fields = ('created_at', 'updated_at',)


class GroupSerializers(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id', 'name')
        read_only_fields = ('created_at', 'updated_at',)


class GroupPermissionsSerializer(serializers.ModelSerializer):
    group = GroupSerializers(read_only=True, required=False)
    group_id = serializers.IntegerField()

    # def validate(self, attrs):
    #     """Validation Function."""
    #     # print self
    #     id = attrs.get('id')
    #     print id
    #     if not id:
    #         g_id = attrs.get('group_id')
    #         modelName = attrs.get('modelname')
    #         ex_date = attrs.get('expire_date')
    #         # print (p_id, s_id, ex_date)
    #         if(GroupPermission.objects.filter(group_id=g_id,
    #                                modelname=modelName).exists()):
    #             raise serializers.ValidationError('Group Permission to this model is Already Exists.')
    #         else:
    #             return attrs

    class Meta:
        model = GroupPermission
        fields = ('id', 'url', 'group', 'modelname', 'add', 'edit', 'delete', 'get',
                  'group_id')
        read_only_fields = ('created_at', 'updated_at',)
        validators = [
            UniqueTogetherValidator(
                queryset=GroupPermission.objects.all(),
                fields=('group_id', 'modelname'),
                message= "Group with this permission is already exist."
            )
        ]


class PasswordSerializer(serializers.ModelSerializer):
    """docstring for PasswordSerializer."""
    password = serializers.CharField(write_only=True, required=False)
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = Account
        fields = ('password', 'confirm_password')
        read_only_fields = ('created_at', 'updated_at',)

class AccountSerializer(serializers.ModelSerializer):
    """docstring for AccountSerializer"""
    group = GroupSerializer(read_only=True, required=False)
    group_id = serializers.IntegerField(required=False)
    password = serializers.CharField(write_only=True, required=False)
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = Account
        fields = ('id', 'group', 'email', 'username',
                  'firstname', 'lastname', 'password', 'confirm_password',
                  'is_admin', 'group_id', 'full_name')
        read_only_fields = ('created_at', 'updated_at',)

        # lookup_field = 'username'
        def get_related_field(self, model_field):
            # Handles initializing the `subcategories` field
            return GroupSerializer()

        def create(self, validated_data):
            print 'create new user'
            return Account.objects.create(**validated_data)
            # serializer = AccountSerializer();
            # return serializer.serialize(account)

        def update(self, instance, validated_data):
            # print (instance)
            # print (self.user)
            instance.username = validated_data.get('username',
                                                   instance.username)
            instance.firstname = validated_data.get('firstname', instance.firstname)
            instance.lastname = validated_data.get('lastname', instance.lastname)
            instance.email = validated_data.get('email', instance.lastname)
            instance.group_id = validated_data.get('group_id', instance.group_id)

            password = validated_data.get('password', None)
            confirm_password = validated_data.get('confirm_password', None)
            print (password , confirm_password , password == confirm_password)

            if password and confirm_password and password == confirm_password:
                instance.set_password(password)
                # instance.save()
            instance.save()

            update_session_auth_hash(self.context.get('request'), instance)
            account = Account.objects.get(pk=instance.id)

            return account


class AccountSerializers(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'username', 'email', 'firstname', 'lastname',
                  'full_name')
        read_only_fields = ('created_at', 'updated_at',)
