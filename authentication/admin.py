from django.contrib import admin
from authentication.models import Account, Group, GroupPermission
from django.contrib.auth.models import Group as MG
# Register your models here.
admin.site.register(Group)
admin.site.unregister(MG)
admin.site.register(Account)
admin.site.register(GroupPermission)
