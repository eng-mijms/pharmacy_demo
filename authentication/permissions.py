# from rest_framework import permissions
# from authentication.models import GroupPermissions
from main.libs import AppPermissions


class AccountPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = 'account'

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class GroupPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = 'group'

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class PermissionPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = 'permission'

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)
