"""Authentication Models."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


# Create your models here.
class Group(models.Model):

    """Group Model."""

    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        """unicode override."""
        return self.name


class AccountManager(BaseUserManager):

    """Group Model."""

    def create_user(self, email=None,
                    password=None, firstname=None,
                    lastname=None, group_id=None, **kwargs):
                            # """override create Method."""
        # group = Group.objects.filter(name=group)
        # print group_id.id
        # return
        # if not email:
        #     raise ValueError('Users must have a valid email address.')

        # if not kwargs.get('username'):
        #     raise ValueError('Users must have a valid username.')

        account = self.model(
            email=self.normalize_email(email), username=kwargs.get('username')
        )

        account.set_password(password)
        account.firstname = firstname
        account.lastname = lastname
        # account.tagline = tagline
        account.group_id = group_id
        # print account.group.id
        account.save()
        # print group_id.id
        # serializers.serialize('json',[account.group,])

        return account

    def create_superuser(self, email, password, **kwargs):
        """create Supe User Override."""
        account = self.create_user(email, password, **kwargs)

        account.is_admin = True
        # account.is_superuser = True
        # account.is_staff = True
        account.save()

        return account


class GroupPermission(models.Model):

    """Group Model."""

    group = models.ForeignKey(Group, related_name="permissions")
    modelname = models.CharField(max_length=40)
    add = models.BooleanField(default=False)
    edit = models.BooleanField(default=False)
    get = models.BooleanField(default=False)
    delete = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def validate_unique(self, exclude=None):
        """Override Validation Method."""
        if(GroupPermission.objects.filter(group_id=self.group_id,
                                         modelname=self.modelname).exists()
           ):
            raise 'Group With this Model Permission is already exist.'


    def __unicode__(self):
        return self.modelname


class Account(AbstractBaseUser):

    """docstring for Account."""

    username = models.CharField(max_length=40, unique=True)
    email = models.EmailField(unique=True, blank=True, null=True)
    firstname = models.CharField(max_length=40, null=True, blank=True)
    lastname = models.CharField(max_length=40, null=True, blank=True)
    # tagline = models.CharField(max_length=140,null=True, blank=True)
    # is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    group = models.ForeignKey(
        Group, null=True, blank=True, related_name='users')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def get_short_name(self):
        return self.username;
        
    @property
    def full_name(self):
        """function override."""
        if self.firstname or self.lastname:
            return ' '.join([self.firstname, self.lastname])
        return ''

    @property
    def is_superuser(self):
        """function override."""
        return self.is_admin

    @property
    def is_staff(self):
        """function override."""
        return self.is_admin

    def has_perm(self, perm, obj=None):
        """function override."""
        return self.is_admin

    def has_module_perms(self, app_label):
        """function override."""
        return self.is_admin

    def __unicode__(self):
        """unicode override."""
        return '%s %s' % (self.username, self.email)
