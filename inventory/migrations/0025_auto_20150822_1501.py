# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0024_itemhistory_operation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='ref_unit',
            field=models.ForeignKey(blank=True, to='inventory.unit', null=True),
        ),
    ]
