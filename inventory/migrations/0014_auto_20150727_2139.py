# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0013_auto_20150727_1349'),
    ]

    operations = [
        migrations.CreateModel(
            name='fabcompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('country', models.CharField(max_length=40)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='package',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='productmaterial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('consentration', models.FloatField(null=True, blank=True)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('material', models.ForeignKey(related_name='material_products', to='inventory.material')),
            ],
        ),
        migrations.CreateModel(
            name='shape',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RenameField(
            model_name='product',
            old_name='ar_name',
            new_name='name_ar',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='eng_name',
            new_name='name_eng',
        ),
        migrations.AddField(
            model_name='product',
            name='package_size',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='reg_num',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='science_name',
            field=models.CharField(max_length=40, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='storage_info',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='productmaterial',
            name='product',
            field=models.ForeignKey(related_name='product_materials', to='inventory.product'),
        ),
        migrations.AddField(
            model_name='productmaterial',
            name='unit',
            field=models.ForeignKey(related_name='unit_materials', to='inventory.unit'),
        ),
        migrations.AddField(
            model_name='product',
            name='fabcompany',
            field=models.ForeignKey(related_name='category_products', blank=True, to='inventory.fabcompany', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='materials',
            field=models.ManyToManyField(to='inventory.material', through='inventory.productmaterial'),
        ),
        migrations.AddField(
            model_name='product',
            name='package',
            field=models.ForeignKey(blank=True, to='inventory.package', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='shape',
            field=models.ForeignKey(blank=True, to='inventory.shape', null=True),
        ),
    ]
