# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0020_auto_20150728_2348'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='unitconvertion',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='unitconvertion',
            name='updated_at',
        ),
        migrations.AlterField(
            model_name='unitconvertion',
            name='unit_from',
            field=models.ForeignKey(related_name='unit_convertions_froms', to='inventory.unit'),
        ),
        migrations.AlterField(
            model_name='unitconvertion',
            name='unit_to',
            field=models.ForeignKey(related_name='unit_convertions_tos', default=0, to='inventory.unit'),
            preserve_default=False,
        ),
    ]
