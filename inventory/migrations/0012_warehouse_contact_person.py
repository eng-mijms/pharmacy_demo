# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0011_warehouse_mobile'),
    ]

    operations = [
        migrations.AddField(
            model_name='warehouse',
            name='contact_person',
            field=models.CharField(max_length=40, null=True, blank=True),
        ),
    ]
