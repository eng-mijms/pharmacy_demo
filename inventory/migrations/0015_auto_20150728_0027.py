# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0014_auto_20150727_2139'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='market_name',
        ),
        migrations.RemoveField(
            model_name='product',
            name='science_name',
        ),
        migrations.AddField(
            model_name='product',
            name='price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_size',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='product_size_unit',
            field=models.ForeignKey(related_name='unit_products', default=0, to='inventory.unit'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='package_size',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
