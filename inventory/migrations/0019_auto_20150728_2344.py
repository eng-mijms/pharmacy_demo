# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0018_auto_20150728_2208'),
    ]

    operations = [
        migrations.CreateModel(
            name='unitconvertion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('unit_to', models.CharField(max_length=200, null=True, blank=True)),
                ('factor', models.FloatField(default=1)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RenameField(
            model_name='product',
            old_name='package',
            new_name='price_unit',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='shape',
            new_name='ref_unit',
        ),
        migrations.AddField(
            model_name='transaction_d',
            name='unit',
            field=models.ForeignKey(related_name='unit_transactin_ds', default=0, to='inventory.unit'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unitconvertion',
            name='product',
            field=models.ForeignKey(related_name='product_units_converted', blank=True, to='inventory.product', null=True),
        ),
        migrations.AddField(
            model_name='unitconvertion',
            name='unit_from',
            field=models.ForeignKey(to='inventory.unit'),
        ),
    ]
