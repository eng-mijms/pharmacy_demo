# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0010_warehouse_tel'),
    ]

    operations = [
        migrations.AddField(
            model_name='warehouse',
            name='mobile',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
