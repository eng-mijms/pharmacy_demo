# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0006_auto_20150725_2121'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bill_h',
            name='items',
            field=models.ManyToManyField(related_name='item_bill_hs', through='inventory.bill_d', to='inventory.item'),
        ),
        migrations.AlterField(
            model_name='item',
            name='sale_price',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='purchase_request_d',
            name='product',
            field=models.ForeignKey(related_name='product_request_ds', to='inventory.product'),
        ),
        migrations.AlterField(
            model_name='purchase_request_h',
            name='products',
            field=models.ManyToManyField(related_name='product_request_hs', through='inventory.purchase_request_d', to='inventory.product'),
        ),
        migrations.AlterField(
            model_name='sal_order_h',
            name='products',
            field=models.ManyToManyField(related_name='product_order_hs', through='inventory.sal_order_d', to='inventory.product'),
        ),
        migrations.AlterField(
            model_name='transaction_h',
            name='items',
            field=models.ManyToManyField(related_name='item_transactions_hs', through='inventory.transaction_d', to='inventory.item'),
        ),
        migrations.AlterField(
            model_name='transaction_h',
            name='sender',
            field=models.ForeignKey(related_name='whid_transaction_senders', blank=True, to='inventory.whid', null=True),
        ),
    ]
