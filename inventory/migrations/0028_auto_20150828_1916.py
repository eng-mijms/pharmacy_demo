# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0027_auto_20150823_2256'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='item',
            unique_together=set([]),
        ),
    ]
