# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0008_auto_20150726_1112'),
    ]

    operations = [
        migrations.AddField(
            model_name='warehouse',
            name='acc_id',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
