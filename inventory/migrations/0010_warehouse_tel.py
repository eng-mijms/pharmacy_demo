# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0009_warehouse_acc_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='warehouse',
            name='tel',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
