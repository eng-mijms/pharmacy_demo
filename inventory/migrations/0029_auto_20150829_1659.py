# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0028_auto_20150828_1916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill_d',
            name='paid',
            field=models.FloatField(default=0),
        ),
        migrations.AlterUniqueTogether(
            name='unitconvertion',
            unique_together=set([]),
        ),
    ]
