# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0029_auto_20150829_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction_d',
            name='cost',
            field=models.FloatField(default=0, null=True, blank=True),
        ),
    ]
