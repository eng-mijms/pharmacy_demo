# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0030_auto_20150829_1708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase_request_d',
            name='quantity',
            field=models.FloatField(default=0),
        ),
    ]
