# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0022_auto_20150731_2030'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='unit',
            field=models.ForeignKey(related_name='unit_items', default=0, to='inventory.unit'),
            preserve_default=False,
        ),
    ]
