# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0025_auto_20150822_1501'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fabcompany',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='material',
            options={'ordering': ['name']},
        ),
        migrations.RenameField(
            model_name='productmaterial',
            old_name='concentration',
            new_name='conc',
        ),
    ]
