# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0012_warehouse_contact_person'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction_h',
            old_name='creator',
            new_name='user',
        ),
        migrations.AddField(
            model_name='transaction_d',
            name='comment',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='barcode',
            name='item',
            field=models.ForeignKey(related_name='item_barcodes', to='inventory.item'),
        ),
    ]
