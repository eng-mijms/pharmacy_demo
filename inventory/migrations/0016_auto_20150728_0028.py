# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0015_auto_20150728_0027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_size_unit',
            field=models.ForeignKey(related_name='unit_products', blank=True, to='inventory.unit', null=True),
        ),
    ]
