# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0017_auto_20150728_1240'),
    ]

    operations = [
        migrations.CreateModel(
            name='itemhistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('bill', models.ForeignKey(blank=True, to='inventory.bill_d', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='operation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_eng', models.CharField(max_length=40, null=True, blank=True)),
                ('name_ar', models.CharField(max_length=40, null=True, blank=True)),
                ('op', models.CharField(max_length=10, choices=[('plus', 'Plus'), ('minus', 'Minus'), ('nutral', 'Nutral')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='bill_h',
            name='saved',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='transaction_h',
            name='saved',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='item',
            name='expire_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='location',
            field=models.ForeignKey(related_name='location_items', blank=True, to='inventory.location', null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='main_barcode',
            field=models.CharField(max_length=40, unique=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='item',
            field=models.ForeignKey(related_name='item_histories', to='inventory.item'),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='operation',
            field=models.ForeignKey(related_name='operation_items_history', to='inventory.operation'),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='transaction',
            field=models.ForeignKey(blank=True, to='inventory.transaction_d', null=True),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='unit',
            field=models.ForeignKey(related_name='unit_items_histories', to='inventory.unit'),
        ),
    ]
