# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0031_auto_20150829_1715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill_h',
            name='customer',
            field=models.ForeignKey(related_name='customer_bills', blank=True, to='inventory.customer', null=True),
        ),
    ]
