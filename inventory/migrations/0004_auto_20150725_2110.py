# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventory', '0003_auto_20150722_2038'),
    ]

    operations = [
        migrations.CreateModel(
            name='transaction_d',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField()),
                ('cost', models.FloatField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='transaction_h',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, null=True, blank=True)),
                ('case', models.IntegerField()),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('creator', models.ForeignKey(related_name='user_transactions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='created',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='items',
        ),
        migrations.AlterField(
            model_name='item',
            name='quantity',
            field=models.FloatField(default=0),
        ),
        migrations.DeleteModel(
            name='transaction',
        ),
        migrations.AddField(
            model_name='transaction_h',
            name='items',
            field=models.ManyToManyField(to='inventory.item', through='inventory.transaction_d'),
        ),
        migrations.AddField(
            model_name='transaction_h',
            name='reciver',
            field=models.ForeignKey(related_name='whid_transaction_recivers', to='inventory.whid'),
        ),
        migrations.AddField(
            model_name='transaction_h',
            name='sender',
            field=models.ForeignKey(related_name='whid_transaction_senders', to='inventory.whid'),
        ),
        migrations.AddField(
            model_name='transaction_d',
            name='item',
            field=models.ForeignKey(related_name='itemn_transactions', to='inventory.item'),
        ),
        migrations.AddField(
            model_name='transaction_d',
            name='transaction',
            field=models.ForeignKey(related_name='transactions', to='inventory.transaction_h'),
        ),
    ]
