# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0026_auto_20150823_1835'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='name_eng',
            field=models.CharField(max_length=40, db_index=True),
        ),
    ]
