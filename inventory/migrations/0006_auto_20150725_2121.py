# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0005_auto_20150725_2116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction_d',
            name='item',
            field=models.ForeignKey(related_name='item_transactions', to='inventory.item'),
        ),
    ]
