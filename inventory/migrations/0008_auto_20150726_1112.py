# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_auto_20150725_2154'),
    ]

    operations = [
        migrations.CreateModel(
            name='warehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_eng', models.CharField(max_length=40)),
                ('name_ar', models.CharField(max_length=40, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('address', models.CharField(max_length=60, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AlterField(
            model_name='purchase_request_h',
            name='reciver',
            field=models.ForeignKey(related_name='whid_purchase_request_recivers', to='inventory.warehouse'),
        ),
        migrations.AlterField(
            model_name='purchase_request_h',
            name='sender',
            field=models.ForeignKey(related_name='whid_purchase_request_senders', to='inventory.warehouse'),
        ),
        migrations.AlterField(
            model_name='transaction_h',
            name='reciver',
            field=models.ForeignKey(related_name='whid_transaction_recivers', to='inventory.warehouse'),
        ),
        migrations.AlterField(
            model_name='transaction_h',
            name='sender',
            field=models.ForeignKey(related_name='whid_transaction_senders', blank=True, to='inventory.warehouse', null=True),
        ),
        migrations.DeleteModel(
            name='whid',
        ),
    ]
