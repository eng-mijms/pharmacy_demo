# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0016_auto_20150728_0028'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productmaterial',
            old_name='consentration',
            new_name='concentration',
        ),
        migrations.AlterField(
            model_name='productmaterial',
            name='unit',
            field=models.ForeignKey(related_name='unit_materials', blank=True, to='inventory.unit', null=True),
        ),
    ]
