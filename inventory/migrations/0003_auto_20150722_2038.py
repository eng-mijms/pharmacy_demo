# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventory', '0002_auto_20150722_0455'),
    ]

    operations = [
        migrations.CreateModel(
            name='bill_d',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('discount', models.FloatField(default=0)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('quantity', models.FloatField()),
                ('paid', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='bill_h',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('customer', models.ForeignKey(related_name='customer_bills', to='inventory.customer')),
            ],
        ),
        migrations.CreateModel(
            name='doctor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=40, null=True, blank=True)),
                ('lastname', models.CharField(max_length=40, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('desc', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='purchase_request_d',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField()),
                ('prefered_expiration', models.DateTimeField(null=True, blank=True)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='purchase_request_h',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.IntegerField(default=0)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='sal_order_d',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField()),
                ('discount', models.FloatField(default=0)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='sal_order_h',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.IntegerField(default=0)),
                ('order_expiration', models.DateTimeField()),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('customer', models.ForeignKey(related_name='customer_sal_orders', to='inventory.customer')),
                ('doctor', models.ForeignKey(related_name='doctor_sal_orders', blank=True, to='inventory.doctor', null=True)),
                ('payment', models.ForeignKey(related_name='payment_sal_orders', to='inventory.payment')),
            ],
        ),
        migrations.CreateModel(
            name='unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('desc', models.CharField(max_length=200, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='whid',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='item',
            name='barcode',
            field=models.CharField(default=1, unique=True, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='cost_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='item_code',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='puchase_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='sale_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='sample',
            field=models.ImageField(null=True, upload_to=b'item/profile', blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='max_disc',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='product',
            name='max_limit',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='product',
            name='min_limit',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='item',
            name='quantity',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='item',
            name='supplier',
            field=models.ForeignKey(related_name='supplier_items', blank=True, to='inventory.supplier', null=True),
        ),
        migrations.AddField(
            model_name='sal_order_h',
            name='products',
            field=models.ManyToManyField(to='inventory.product', through='inventory.sal_order_d'),
        ),
        migrations.AddField(
            model_name='sal_order_h',
            name='user',
            field=models.ForeignKey(related_name='user_sal_orders', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='sal_order_d',
            name='order',
            field=models.ForeignKey(related_name='order_ds', to='inventory.sal_order_h'),
        ),
        migrations.AddField(
            model_name='sal_order_d',
            name='product',
            field=models.ForeignKey(related_name='item_order_ds', to='inventory.product'),
        ),
        migrations.AddField(
            model_name='sal_order_d',
            name='unit',
            field=models.ForeignKey(related_name='unit_order_ds', to='inventory.unit'),
        ),
        migrations.AddField(
            model_name='purchase_request_h',
            name='products',
            field=models.ManyToManyField(to='inventory.product', through='inventory.purchase_request_d'),
        ),
        migrations.AddField(
            model_name='purchase_request_h',
            name='reciver',
            field=models.ForeignKey(related_name='whid_purchase_request_recivers', to='inventory.whid'),
        ),
        migrations.AddField(
            model_name='purchase_request_h',
            name='sender',
            field=models.ForeignKey(related_name='whid_purchase_request_senders', to='inventory.whid'),
        ),
        migrations.AddField(
            model_name='purchase_request_h',
            name='supplier',
            field=models.ForeignKey(related_name='supplier_orders', blank=True, to='inventory.supplier', null=True),
        ),
        migrations.AddField(
            model_name='purchase_request_h',
            name='user',
            field=models.ForeignKey(related_name='user_purchase_orders', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='purchase_request_d',
            name='product',
            field=models.ForeignKey(related_name='item_request_ds', to='inventory.product'),
        ),
        migrations.AddField(
            model_name='purchase_request_d',
            name='purchase_order',
            field=models.ForeignKey(related_name='purchase_orders', to='inventory.purchase_request_h'),
        ),
        migrations.AddField(
            model_name='purchase_request_d',
            name='unit',
            field=models.ForeignKey(related_name='unit_request_ds', to='inventory.unit'),
        ),
        migrations.AddField(
            model_name='bill_h',
            name='doctor',
            field=models.ForeignKey(related_name='doctor_orders', blank=True, to='inventory.doctor', null=True),
        ),
        migrations.AddField(
            model_name='bill_h',
            name='items',
            field=models.ManyToManyField(to='inventory.item', through='inventory.bill_d'),
        ),
        migrations.AddField(
            model_name='bill_h',
            name='payment',
            field=models.ForeignKey(related_name='payment_bills', to='inventory.payment'),
        ),
        migrations.AddField(
            model_name='bill_h',
            name='user',
            field=models.ForeignKey(related_name='user_bills', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bill_d',
            name='bill',
            field=models.ForeignKey(related_name='bill_ds', to='inventory.bill_h'),
        ),
        migrations.AddField(
            model_name='bill_d',
            name='item',
            field=models.ForeignKey(related_name='item_bill_ds', to='inventory.item'),
        ),
        migrations.AddField(
            model_name='bill_d',
            name='unit',
            field=models.ForeignKey(related_name='unit_bill_ds', to='inventory.unit'),
        ),
    ]
