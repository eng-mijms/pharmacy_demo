# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0021_auto_20150729_0140'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='unit',
            options={'ordering': ['name']},
        ),
        migrations.RemoveField(
            model_name='itemhistory',
            name='operation',
        ),
        migrations.RemoveField(
            model_name='transaction_h',
            name='case',
        ),
        migrations.AddField(
            model_name='transaction_h',
            name='operation',
            field=models.ForeignKey(related_name='operation_transactions', default=0, to='inventory.operation'),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='unitconvertion',
            unique_together=set([('product', 'unit_from', 'unit_to')]),
        ),
    ]
