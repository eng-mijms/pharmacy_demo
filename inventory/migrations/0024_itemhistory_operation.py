# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0023_item_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemhistory',
            name='operation',
            field=models.ForeignKey(related_name='operation_items_history', default=0, to='inventory.operation'),
            preserve_default=False,
        ),
    ]
