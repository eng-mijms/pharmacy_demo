# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from inventory.models import *
from inventory.models import unit, product, item
# from django import forms
# from django.forms import ModelForm
# Register your models here.


# class UnitAdminForm(forms.ModelForm):
#     def __init__(self, *args, **kwargs):
#         super(UnitAdminForm, self).__init__(*args, **kwargs)
#         print item
#         print unitconv
#         self.fields['unit'].queryset = unit.objects.all()
# class itemunits(admin.ModelAdmin):
#     # item = item.objects.get(pk=item)
#     # forms.unit.queryset( unit.objects.filter(id=1))
#     form = UnitAdminForm
#     # fields = ('item', 'transaction', 'quantity', 'unit', 'cost')
admin.site.register(location,)
admin.site.register(unitconvertion,)
admin.site.register(doctor,)
admin.site.register(payment,)
admin.site.register(operation,)
admin.site.register(itemhistory,)
admin.site.register(unit,)
admin.site.register(category,)
admin.site.register(supplier,)
admin.site.register(customer,)
admin.site.register(product,)
admin.site.register(item,)
admin.site.register(transaction_h,)
admin.site.register(transaction_d,)
admin.site.register(sal_order_h,)
admin.site.register(sal_order_d,)
admin.site.register(bill_h,)
admin.site.register(bill_d,)
admin.site.register(barcode,)
admin.site.register(warehouse,)
admin.site.register(purchase_request_h,)
admin.site.register(purchase_request_d,)
admin.site.register(productmaterial,)
