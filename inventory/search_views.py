"""Search Serializers View."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from inventory.serializers import ProductsSerializers, ItemsSerializers, UnitSerializer
from rest_framework import generics, status
from inventory.permissions import ProductPermissions, ItemPermissions, UnitPermissions
from inventory.models import product, item, unit
from rest_framework.response import Response
from django.db.models import Q


class SearchLib(generics.ListAPIView):

    """Search Class."""

    limitation = 30

    class SearchSettings():
        model = {}
        query = ''
        queryParam = 'q'

    def list(self, request, format=None):
        """return Searched List."""
        # print request.data
        # print 'hi'
        try:
            # print request.query_params['q']
            q = request.query_params[self.SearchSettings.queryParam]
            dQuery = {self.SearchSettings.query: q}
            data = self.SearchSettings.model.objects.filter(Q(**dQuery))
            data = data[0:self.limitation]
        except Exception:
            # raise e
            data = self.SearchSettings.model.objects.all()[0:self.limitation]
        context = {'request': request}
        serializer = self.serializer_class(data, context=context, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductSearch(SearchLib):

    """Product Search Class."""

    # queryset = product.objects.all()
    permission_classes = (ProductPermissions, )
    serializer_class = ProductsSerializers
    limitation = 10

    class SearchSettings(SearchLib.SearchSettings):
        model = product
        query = 'name_eng__icontains'


class ItemSearch(SearchLib):

    """Item Search Class."""

    # queryset = product.objects.all()
    permission_classes = (ItemPermissions, )
    serializer_class = ItemsSerializers
    limitation = 10

    class SearchSettings(SearchLib.SearchSettings):
        model = item
        query = 'product__name_eng__icontains'


class UnitConversionProductSearch(SearchLib):

    """Item Search Class."""

    # queryset = product.objects.all()
    permission_classes = (UnitPermissions, )
    serializer_class = UnitSerializer
    limitation = 10

    class SearchSettings(SearchLib.SearchSettings):
        model = unit
        query = 'unit_convertions_froms__product_id'
