"""Inventory Views."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import permissions, viewsets, status
from .models import *
from .serializers import *
from .permissions import *
from django.db import transaction as DjTrans
from rest_framework.response import Response
from rest_framework.decorators import detail_route

class LocationViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = location.objects.all()
    serializer_class = LocationsSerializers
    permission_classes = (permissions.IsAuthenticated, LocationPermissions,)


class UnitViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = unit.objects.all()
    serializer_class = UnitSerializer
    permission_classes = (permissions.IsAuthenticated, UnitPermissions,)


class CategoryViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = category.objects.all()
    serializer_class = CategoriesSerializers
    permission_classes = (permissions.IsAuthenticated, CategoryPermissions,)


class MaterialViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = material.objects.all()
    serializer_class = MaterialsSerializers
    permission_classes = (permissions.IsAuthenticated, MaterialPermissions,)


class ShapeViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = shape.objects.all()
    serializer_class = ShapesSerializers
    permission_classes = (permissions.IsAuthenticated, ShapePermissions,)


class PackageViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = package.objects.all()
    serializer_class = PackagesSerializers
    permission_classes = (permissions.IsAuthenticated, PackagePermissions,)


class FabCompanyViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = fabcompany.objects.all()
    serializer_class = FabCompniesSerializers
    permission_classes = (permissions.IsAuthenticated, FabCompanyPermissions,)


class SupplierViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = supplier.objects.all()
    serializer_class = SuppliersSerializers
    permission_classes = (permissions.IsAuthenticated, SupplierPermissions,)


class CustomerViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = customer.objects.all()
    serializer_class = CustomersSerializers
    permission_classes = (permissions.IsAuthenticated, CustomerPermissions,)


class ProductViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = product.objects.all()
    serializer_class = ProductsSerializers
    permission_classes = (permissions.IsAuthenticated, ProductPermissions,)

    def create(self, request):
        """Override Create Method."""
        materials = request.data.pop("materials")
        with DjTrans.atomic():
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                newProduct = product.objects.create(
                    **serializer.validated_data)
                for material in materials:
                    tempMaterial = material.pop('material')
                    print tempMaterial
                    material['product'] = newProduct
                    material['material_id'] = tempMaterial[u'id']
                    productmaterial.objects.create(**material)
                serialize = self.serializer_class(newProduct,
                                                  context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response({
            'status': 'Create Error',
            'message': 'Data sending error.'
        }, status=status.HTTP_400_BAD_REQUEST)


class ItemViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = item.objects.all()
    serializer_class = ItemsSerializers
    permission_classes = (permissions.IsAuthenticated, ItemPermissions,)


class BarcodeViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = barcode.objects.all()
    serializer_class = BarcodesSerializers
    permission_classes = (permissions.IsAuthenticated, BarcodePermissions,)

    def perform_create(self, serializer):
        """add user to Create Method."""
        serializer.save(owner=self.request.user)


class PaymentViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = payment.objects.all()
    serializer_class = PaymentsSerializers
    permission_classes = (permissions.IsAuthenticated, PaymentPermissions,)


class DoctorViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = doctor.objects.all()
    serializer_class = DoctorsSerializers
    permission_classes = (permissions.IsAuthenticated, DoctorPermissions,)


class UnitConvertionViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = unitconvertion.objects.all()
    serializer_class = UnitCoversionsSerializers
    permission_classes = (UnitConversionPermissions,)


class WareHouseViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = warehouse.objects.all()
    serializer_class = WareHousessSerializers
    permission_classes = (permissions.IsAuthenticated, WareHousePermissions,)


class BillViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = bill_h.objects.all()
    serializer_class = BillsSerializers
    permission_classes = (permissions.IsAuthenticated, BillPermissions,)


    @detail_route(methods=['post', 'put'])
    def confirm(self, request, pk=None):
        instance = self.get_object()
        instance.saved = request.data.pop("saved")
        instance.save()
        serialize = self.serializer_class(instance,
                                          context={'request': request})
        return Response(serialize.data, status=status.HTTP_201_CREATED)

    def create(self, request):
        """Override Create Method."""
        bill_ds = request.data.pop("bill_ds")
        if('saved' in request.data):
            print 'saved founded'
            saved = request.data.pop("saved")
        else:
            saved = None
        with DjTrans.atomic():
            # print request.user
            request.data['user_id'] = request.user.id
            serializer = self.serializer_class(data=request.data)
            # print request.data
            # print serializer.is_valid()
            if serializer.is_valid():
                newBill = bill_h.objects.create(
                    **serializer.validated_data)
                for bill in bill_ds:
                    tempItem = bill.pop('item')
                    # print tempMaterial
                    bill['bill'] = newBill
                    bill['item_id'] = tempItem[u'id']
                    bill_d.objects.create(**bill)
                if (saved):
                    newBill.saved = saved
                    newBill.save()
                serialize = self.serializer_class(newBill,
                                                  context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        instance = self.get_object()
        # print oldTrans
        bill_ds = request.data.pop("bill_ds")
        # print instance
        # request.data.pop("user")
        # request.data.pop("user_id")
        with DjTrans.atomic():
            instance.bill_ds.all().delete()
            # print request.user
            # request.data['user_id'] = request.user.id
            # print request.data
            serializer = self.serializer_class(data=request.data)
            # print request.data
            # print serializer.is_valid()
            if serializer.is_valid():
                for bill in bill_ds:
                    tempItem = bill.pop('item')
                    # print tempMaterial
                    bill['bill'] = instance
                    if('unit' in bill):
                        bill.pop('unit')
                    bill['item_id'] = tempItem[u'id']
                    # print serializer
                    print 'before'
                    print bill
                    bill_d.objects.create(**bill)
                    print 'after'
                validated_data = serializer.validated_data
                # instance.title = validated_data['title']
                instance.payment_id = validated_data['payment_id']
                instance.doctor_id = validated_data['doctor_id']
                instance.customer_id = validated_data['customer_id']
                instance.comment = validated_data['comment']
                # instance.saved = validated_data['saved']
                instance.save()
                # print serializer.validated_data
                # print serializer.validated_data[u'user_id']
                serialize = self.serializer_class(instance,
                                                  context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
class SaleOrderViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = sal_order_h.objects.all()
    serializer_class = SaleOrdersSerializers
    permission_classes = (permissions.IsAuthenticated, SaleOrderPermissions,)


class PurchaseRequestViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = purchase_request_h.objects.all()
    serializer_class = PurchaseRequestsSerializers
    permission_classes = (permissions.IsAuthenticated, PurchasePermissions,)


class TransactionViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = transaction_h.objects.all()
    serializer_class = TransactionsSerializers
    permission_classes = (permissions.IsAuthenticated, TransactionPermissions,)

    @detail_route(methods=['post', 'put'])
    def confirm(self, request, pk=None):
        instance = self.get_object()
        instance.saved = request.data.pop("saved")
        instance.save()
        serialize = self.serializer_class(instance,
                                          context={'request': request})
        return Response(serialize.data, status=status.HTTP_201_CREATED)

    def create(self, request):
        """Override Create Method."""
        trans_ds = request.data.pop("transactions")
        if('saved' in request.data):
            # print 'saved founded'
            saved = request.data.pop("saved")
        else:
            saved = None
        with DjTrans.atomic():
            # print request.user
            request.data['user_id'] = request.user.id
            serializer = self.serializer_class(data=request.data)
            # print request.data
            # print serializer.is_valid()
            if serializer.is_valid():
                newTran = transaction_h.objects.create(
                    **serializer.validated_data)
                for tran in trans_ds:
                    tempItem = tran.pop('item')
                    # print tempMaterial
                    tran['transaction'] = newTran
                    tran['item_id'] = tempItem[u'id']
                    transaction_d.objects.create(**tran)
                if (saved):
                    newTran.saved = saved
                    newTran.save()
                serialize = self.serializer_class(newTran,
                                                  context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        instance = self.get_object()
        # print oldTrans
        trans_ds = request.data.pop("transactions")
        print instance
        # request.data.pop("user")
        # request.data.pop("user_id")
        with DjTrans.atomic():
            instance.transactions.all().delete()
            # print request.user
            # request.data['user_id'] = request.user.id
            # print request.data
            serializer = self.serializer_class(data=request.data)
            # print request.data
            # print serializer.is_valid()
            if serializer.is_valid():
                for tran in trans_ds:
                    tempItem = tran.pop('item')
                    # print tempMaterial
                    tran['transaction'] = instance
                    if('unit' in tran):
                        tran.pop('unit')
                    tran['item_id'] = tempItem[u'id']
                    # print serializer
                    transaction_d.objects.create(**tran)
                validated_data = serializer.validated_data
                instance.title = validated_data['title']
                instance.sender_id = validated_data['sender_id']
                instance.reciver_id = validated_data['reciver_id']
                instance.operation_id = validated_data['operation_id']
                instance.comment = validated_data['comment']
                # instance.saved = validated_data['saved']
                instance.save()
                # print serializer.validated_data
                # print serializer.validated_data[u'user_id']
                serialize = self.serializer_class(instance,
                                                  context={'request': request})
                return Response(serialize.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OperationViewSet(viewsets.ModelViewSet):

    """Model Serializer View."""

    queryset = operation.objects.all()
    serializer_class = OperationsSerializer
    permission_classes = (permissions.IsAuthenticated,)
