"""Inventory Model Urls."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, url
from rest_framework import routers
# from django.contrib import admin
from inventory.views import *
from inventory import search_views as sViews


router = routers.DefaultRouter()
router.register(r'locations', LocationViewSet)
router.register(r'units', UnitViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'shapes', ShapeViewSet)
router.register(r'packages', PackageViewSet)
router.register(r'fab_companies', FabCompanyViewSet)
router.register(r'materials', MaterialViewSet)
router.register(r'suppliers', SupplierViewSet)
router.register(r'customers', CustomerViewSet)
router.register(r'products', ProductViewSet)
router.register(r'items', ItemViewSet)
router.register(r'barcodes', BarcodeViewSet)
router.register(r'payments', PaymentViewSet)
router.register(r'doctors', DoctorViewSet)
router.register(r'warehouses', WareHouseViewSet)
router.register(r'bills', BillViewSet)
router.register(r'sale_orders', SaleOrderViewSet)
router.register(r'purchase_requests', PurchaseRequestViewSet)
router.register(r'transactions', TransactionViewSet)
router.register(r'operations', OperationViewSet)
router.register(r'unit_conversions', UnitConvertionViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^search/product/', sViews.ProductSearch.as_view()),
    url(r'^search/item/', sViews.ItemSearch.as_view()),
    url(r'^search/unit/conversion/product/', sViews.UnitConversionProductSearch.as_view()),
    # url(r'^api/users/$', AccountViewSet.as_view())
    # url('^.*$', views.index , name="index"),
]
