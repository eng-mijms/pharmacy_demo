"""Inventory Serializers."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.contrib.auth import update_session_auth_hash
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
# from rest_framework.renderers import JSONRenderer
from inventory.models import location, material, category, shape, package, \
    fabcompany, productmaterial, supplier, customer, product, item, barcode, \
    payment, doctor, warehouse, bill_h, bill_d, sal_order_d, sal_order_h, \
    transaction_d, transaction_h, purchase_request_h, purchase_request_d, unit,\
    unitconvertion, operation
from main.libs import Base64ImageField
from authentication.serializers import AccountSerializers


class UnitSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = unit
        fields = ('id', 'url', 'name', 'desc',)
        read_only_fields = ('created_at', 'updated_at',)


class CategorySerializer(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = category
        fields = ('id', 'url', 'name', 'desc',)
        read_only_fields = ('created_at', 'updated_at',)


class MaterialsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = material
        fields = ('id', 'url', 'name', 'comment',)
        read_only_fields = ('created_at', 'updated_at',)


class ShapesSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = shape
        fields = ('id', 'url', 'name', 'comment',)
        read_only_fields = ('created_at', 'updated_at',)


class PackagesSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = package
        fields = ('id', 'url', 'name', 'comment',)
        read_only_fields = ('created_at', 'updated_at',)


class FabCompniesSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = fabcompany
        fields = ('id', 'url', 'name', 'country', 'comment',)
        read_only_fields = ('created_at', 'updated_at',)


class ProductMaterialSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    unit = UnitSerializer(read_only=True)
    unit_id = serializers.IntegerField(allow_null=True, required=False)
    material = MaterialsSerializers(read_only=True)
    material_id = serializers.IntegerField()

    class Meta:
        model = productmaterial
        fields = ('id', 'unit_id', 'unit', 'material',
                  'material_id', 'conc', 'comment')
        read_only_fields = ('created_at', 'updated_at',)

class unitConversionSerializer(serializers.ModelSerializer):

    """Unit Conersion Lazy Serializer."""

    unit_from = UnitSerializer(read_only=True)
    unit_to = UnitSerializer(read_only=True)

    class Meta:
        model = unitconvertion
        fields = ('id', 'url', 'unit_from', 'unit_to',
                   'factor')

class ProductSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    ref_unit = UnitSerializer(read_only=True)
    product_size_unit = UnitSerializer(read_only=True)
    price_unit = UnitSerializer(read_only=True)
    category = CategorySerializer(read_only=True)
    fabcompany = FabCompniesSerializers(read_only=True)
    product_materials = ProductMaterialSerializer(many=True, read_only=True)
    # product_units_converted = unitConversionSerializer(read_only=True, many=True)

    class Meta:
        model = product
        fields = ('id', 'url', 'reg_num', 'name_eng', 'name_ar',
                  'product_materials', 'price', 'product_size', 'desc',
                  'category', 'price_unit',
                  'ref_unit', 'package_size', 'product_size_unit',
                  'fabcompany', 'storage_info', 'priority', 'min_limit',
                  'max_limit', 'max_disc', 'product_units_converted')
        read_only_fields = ('created_at', 'updated_at',)


class UnitCoversionsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    product = ProductSerializer(read_only=True)
    product_id = serializers.IntegerField()
    unit_from = UnitSerializer(read_only=True)
    unit_from_id = serializers.IntegerField()
    unit_to = UnitSerializer(read_only=True)
    unit_to_id = serializers.IntegerField()

    class Meta:
        model = unitconvertion
        fields = ('id', 'url', 'product', 'product_id',
                  'unit_from', 'unit_from_id', 'unit_to',
                  'unit_to_id', 'factor')
        validators = [
            UniqueTogetherValidator(
                queryset=unitconvertion.objects.all(),
                fields=('product_id', 'unit_from_id', 'unit_to_id'),
                message= "Conversion to This Product with this Units already exist."
            )
        ]



class BarcodeSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    owner = AccountSerializers()

    class Meta:
        model = barcode
        fields = ('id', 'url', 'owner', 'barcode')
        read_only_fields = ('created_at', 'updated_at',)


class LocationsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = location
        fields = ('id', 'url', 'name', 'desc', 'capacity',)
        read_only_fields = ('created_at', 'updated_at',)


class CategoriesSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)
    category_products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = category
        fields = ('id', 'url', 'name', 'desc', 'category_products')
        read_only_fields = ('created_at', 'updated_at',)


class SuppliersSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = supplier
        fields = ('id', 'url', 'name', 'tel', 'address',
                  'email', 'priority', 'comment')
        read_only_fields = ('created_at', 'updated_at',)


class CustomersSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    # group_id = serializers.IntegerField()
    # group = GroupSerializer(read_only=True)

    class Meta:
        model = customer
        fields = ('id', 'url', 'name', 'tel', 'address',
                  'email', 'priority', 'comment')
        read_only_fields = ('created_at', 'updated_at',)


class ProductsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    category = CategorySerializer(read_only=True)
    category_id = serializers.IntegerField()
    ref_unit = UnitSerializer(read_only=True)
    ref_unit_id = serializers.IntegerField()
    product_size_unit = UnitSerializer(read_only=True)
    product_size_unit_id = serializers.IntegerField()
    price_unit = UnitSerializer(read_only=True)
    price_unit_id = serializers.IntegerField()
    fabcompany = FabCompniesSerializers(read_only=True)
    fabcompany_id = serializers.IntegerField()
    product_materials = ProductMaterialSerializer(many=True, read_only=True)
    materials = serializers.PrimaryKeyRelatedField(
        many=True,
        required=False, queryset=material.objects.all())
    # material_detail = MaterialsSerializers(many=True, read_only=True)

    class Meta:
        model = product
        fields = ('id', 'url', 'reg_num', 'name_eng', 'name_ar', 'materials',
                  'product_materials', 'price', 'product_size', 'desc',
                  'category', 'category_id', 'price_unit', 'price_unit_id',
                  'ref_unit', 'ref_unit_id', 'package_size',
                  'product_size_unit', 'product_size_unit_id',
                  'fabcompany', 'fabcompany_id', 'storage_info', 'priority',
                  'min_limit', 'max_limit', 'max_disc')
        read_only_fields = ('created_at', 'updated_at',)


class ItemSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    product = ProductsSerializers(read_only=True)
    supplier = SuppliersSerializers(read_only=True)
    location = LocationsSerializers(read_only=True)
    unit = UnitSerializer(read_only=True)

    class Meta:
        model = item
        fields = ('id', 'url', 'product', 'supplier', 'location', 'expire_date',
                  'cost_price', 'puchase_price', 'price', 'sale_price',
                  'main_barcode', 'item_code', 'sample', 'quantity',
                  'comment', 'unit')
        read_only_fields = ('created_at', 'updated_at',)


class ItemsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    product = ProductsSerializers(read_only=True)
    product_id = serializers.IntegerField()
    supplier = SuppliersSerializers(read_only=True)
    supplier_id = serializers.IntegerField(required=False)
    location = LocationsSerializers(read_only=True)
    location_id = serializers.IntegerField(required=False)
    # sample = Base64ImageField(required=False)
    item_barcodes = BarcodeSerializer(many=True, read_only=True)
    unit = UnitSerializer(read_only=True)
    unit_id = serializers.IntegerField()

    def validate(self, attrs):
        """Validation Function."""
        p_id = attrs.get('product_id')
        s_id = attrs.get('supplier_id')
        ex_date = attrs.get('expire_date')
        # print (p_id, s_id, ex_date)
        if(item.objects.filter(product_id=p_id,
                               supplier_id=s_id,
                               expire_date=ex_date).exists()):
            raise serializers.ValidationError('Product with this Expiration and\
                                                supplier already exist')
        else:
            return attrs

    class Meta:
        model = item
        fields = ('id', 'url', 'product', 'product_id', 'supplier',
                  'supplier_id', 'location', 'location_id', 'expire_date',
                  'cost_price', 'puchase_price', 'price', 'sale_price',
                  'main_barcode', 'item_code', 'sample', 'quantity',
                  'comment', 'item_barcodes', 'unit', 'unit_id'
                  )
        read_only_fields = ('created_at', 'updated_at',)


class BarcodesSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    owner = AccountSerializers(read_only=True)
    item = ItemSerializer(read_only=True)
    item_id = serializers.IntegerField()

    class Meta:
        model = barcode
        fields = ('id', 'url', 'barcode', 'owner', 'item', 'item_id')
        read_only_fields = ('created_at', 'updated_at',)


class PaymentsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    class Meta:
        model = payment
        fields = ('id', 'name', 'desc')
        read_only_fields = ('created_at', 'updated_at',)


class DoctorsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    class Meta:
        model = doctor
        fields = ('id', 'firstname', 'lastname')
        read_only_fields = ('created_at', 'updated_at',)


class WareHousessSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    class Meta:
        model = warehouse
        fields = ('id', 'name_eng', 'name_ar', 'email',
                  'address', 'tel', 'mobile', 'acc_id')
        read_only_fields = ('created_at', 'updated_at',)


class BillDetailSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    item = ItemSerializer(read_only=True)
    unit = UnitSerializer(read_only=True)
    unit_id = serializers.IntegerField()

    class Meta:
        model = bill_d
        fields = ('id', 'item', 'unit', 'discount', 'comment',
                  'quantity', 'unit', 'unit_id', 'paid', 'price')
        # read_only_fields = ('created_at', 'updated_at',)


class BillsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    bill_ds = BillDetailSerializer(many=True, read_only=True)
    user = AccountSerializers(read_only=True)
    user_id = serializers.IntegerField()
    doctor = DoctorsSerializers(read_only=True)
    doctor_id = serializers.IntegerField(allow_null=True, required=False)
    payment = PaymentsSerializers(read_only=True)
    payment_id = serializers.IntegerField()
    customer = CustomersSerializers(read_only=True)
    customer_id = serializers.IntegerField(allow_null=True, required=False)

    class Meta:
        model = bill_h
        fields = ('id', 'bill_ds', 'user', 'user_id', 'doctor', 'doctor_id',
                  'payment', 'payment_id', 'customer', 'customer_id', 'comment', 'saved')
        read_only_fields = ('created_at', 'updated_at',)


class SaleOrderDetailSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    product = ProductSerializer(read_only=True)
    unit = UnitSerializer(read_only=True)

    class Meta:
        model = sal_order_d
        fields = (
            'product', 'discount', 'comment', 'quantity', 'unit')
        # read_only_fields = ('created_at', 'updated_at',)


class SaleOrdersSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    order_ds = SaleOrderDetailSerializer(many=True, read_only=True)
    user = AccountSerializers(read_only=True)
    user_id = serializers.IntegerField()
    doctor = DoctorsSerializers(read_only=True)
    doctor_id = serializers.IntegerField(allow_null=True, required=False)
    payment = PaymentsSerializers(read_only=True)
    payment_id = serializers.IntegerField()
    customer = CustomersSerializers(read_only=True)
    customer_id = serializers.IntegerField()

    class Meta:
        model = sal_order_h
        fields = ('id', 'order_ds', 'user', 'doctor',
                  'doctor_id', 'payment',
                  'payment_id', 'customer', 'customer_id', 'comment', 'state',
                  'order_expiration')
        read_only_fields = ('created_at', 'updated_at',)


class PurchaseRequestDetailSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    product = ProductSerializer(read_only=True)
    # product_id = serializers.IntegerField(write_only=True)
    unit = UnitSerializer(read_only=True)

    class Meta:
        model = purchase_request_d
        fields = ('id', 'product', 'quantity', 'comment',
                  'unit', 'prefered_expiration')
        # read_only_fields = ('created_at', 'updated_at',)


class PurchaseRequestsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    purchase_orders = PurchaseRequestDetailSerializer(
        many=True, read_only=True)
    user = AccountSerializers(read_only=True)
    user_id = serializers.IntegerField()
    supplier = SuppliersSerializers(read_only=True)
    supplier_id = serializers.IntegerField(allow_null=True, required=False)
    sender = WareHousessSerializers(read_only=True)
    sender_id = serializers.IntegerField()
    reciver = WareHousessSerializers(read_only=True)
    reciver_id = serializers.IntegerField()

    class Meta:
        model = purchase_request_h
        fields = ('id', 'purchase_orders', 'user', 'supplier',
                  'supplier_id', 'sender', 'sender_id', 'reciver',
                  'reciver_id', 'comment', 'state')
        read_only_fields = ('created_at', 'updated_at',)


class OperationsSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    class Meta:
        model = operation
        fields = ('id', 'name_eng', 'name_ar', 'op')

class TransactionDetailSerializer(serializers.ModelSerializer):

    """Model Serializer."""

    item = ItemSerializer(read_only=True)
    unit = UnitSerializer(read_only=True)
    unit_id = serializers.IntegerField()

    class Meta:
        model = transaction_d
        fields = ('id', 'item', 'unit', 'quantity', 'cost', 'comment', 'unit_id')
        # read_only_fields = ('created_at', 'updated_at',)


class TransactionsSerializers(serializers.ModelSerializer):

    """Model Serializer."""

    transactions = TransactionDetailSerializer(many=True, read_only=True)
    user = AccountSerializers(read_only=True)
    user_id = serializers.IntegerField()
    sender = WareHousessSerializers(read_only=True)
    sender_id = serializers.IntegerField(allow_null=True, required=False)
    reciver = WareHousessSerializers(read_only=True)
    reciver_id = serializers.IntegerField()
    operation = OperationsSerializer(read_only=True)
    operation_id = serializers.IntegerField()

    def update(self, instance, validated_data):
    # Update the book instance
        # print instance
        # print validated_data
        instance.title = validated_data['title']
        instance.sender_id = validated_data['sender_id']
        instance.reciver_id = validated_data['reciver_id']
        instance.operation_id = validated_data['operation_id']
        instance.comment = validated_data['comment']
        instance.save()

        return instance

    class Meta:
        model = transaction_h
        fields = ('id', 'transactions', 'user', 'user_id', 'sender',
                  'sender_id', 'reciver', 'reciver_id', 'comment',
                  'operation', 'operation_id', 'saved', 'title')
        read_only_fields = ('created_at', 'updated_at',)
