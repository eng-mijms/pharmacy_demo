# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from rest_framework import permissions
from main.libs import AppPermissions


class LocationPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "location"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class CategoryPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "category"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class SupplierPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "supplier"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class UnitConversionPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "unit_conversions"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class CustomerPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "customer"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class ProductPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "product"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class ItemPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "item"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class BarcodePermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "barcode"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class PaymentPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "payment"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class DoctorPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "doctor"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class WareHousePermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "warehouse"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class BillPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "bill"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class SaleOrderPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "sale order"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class PurchasePermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "purchase order"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class TransactionPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "transaction"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class UnitPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "unit"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class MaterialPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "material"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class ShapePermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "shape"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class PackagePermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "package"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class FabCompanyPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "fab_company"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)


class OperationPermissions(AppPermissions):
    class Mysettings(AppPermissions.Mysettings):
        modelname = "operation"

    def has_permission(self, request, view):
        return AppPermissions.permission(self, request, view)

    def has_object_permission(self, request, view, account):
        return AppPermissions.permission(self, request, view, account)
