"""Inventory App models."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from authentication.models import Account
from django.db import transaction as DjTrans
# Create your models here.


class location(models.Model):

    """Loccation Model."""

    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200, blank=True, null=True)
    capacity = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return 'name= %s, capacity= %s' % (self.name, self.capacity)


class category(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return 'name= %s' % (self.name, )


class unit(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40, unique=True)
    desc = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class supplier(models.Model):

    """Supplier Model."""

    name = models.CharField(max_length=40)
    tel = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=80, blank=True, null=True)
    priority = models.IntegerField(default=0)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class customer(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40)
    tel = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=80, blank=True, null=True)
    priority = models.IntegerField(default=0)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class material(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class shape(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class package(models.Model):

    """Model Class."""

    name = models.CharField(max_length=40)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class fabcompany(models.Model):

    """Fabricate Company Model."""

    name = models.CharField(max_length=40)
    country = models.CharField(max_length=40)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name


class product(models.Model):

    """Product Model."""

    category = models.ForeignKey(
        category, null=True, blank=True, related_name='category_products')
    fabcompany = models.ForeignKey(
        fabcompany, related_name='category_products', null=True, blank=True)
    name_eng = models.CharField(max_length=40, db_index=True)
    name_ar = models.CharField(max_length=40, blank=True, null=True)
    # market_name = models.CharField(max_length=40, blank=True, null=True)
    # science_name = models.CharField(max_length=40, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)  # for linking pupose only
    ref_unit = models.ForeignKey(unit, blank=True, null=True)
    product_size = models.IntegerField(blank=True, null=True)
    product_size_unit = models.ForeignKey(
        unit, related_name='unit_products', null=True, blank=True)
    reg_num = models.CharField(max_length=20, null=True, blank=True)
    materials = models.ManyToManyField(material, through='productmaterial')
    price_unit = models.ForeignKey(package, blank=True, null=True)
    package_size = models.IntegerField(blank=True, null=True)
    priority = models.IntegerField(default=0)
    min_limit = models.FloatField(default=0)
    max_limit = models.FloatField(default=0)
    max_disc = models.FloatField(default=0)
    storage_info = models.CharField(max_length=100, blank=True, null=True)
    desc = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name_eng


class unitconvertion(models.Model):

    """Unit Conversion Model."""

    product = models.ForeignKey(
        product, null=True, blank=True, related_name="product_units_converted")
    unit_from = models.ForeignKey(unit, related_name="unit_convertions_froms")
    unit_to = models.ForeignKey(unit, related_name="unit_convertions_tos")
    factor = models.FloatField(default=1)

    # class Meta:
    #     unique_together = ('product', 'unit_from', 'unit_to')

    def validate_unique(self, exclude=None):
        """Override Validation Method."""
        if(unitconvertion.objects.filter(product_id=self.product_id,
                                         unit_from_id=self.unit_to_id,
                                         unit_to_id=self.unit_from_id).exists()
           ):
            raise 'Product with this Units conversion is Exist'

    def __unicode__(self):
        """Override Unicode Function."""
        return "from %s - to %s" % (self.unit_from.name, self.unit_to.name,)


class productmaterial(models.Model):

    """Product Materials Link Model."""

    material = models.ForeignKey(material, related_name='material_products')
    product = models.ForeignKey(product, related_name='product_materials')
    unit = models.ForeignKey(
        unit, related_name='unit_materials', blank=True, null=True)
    conc = models.FloatField(null=True, blank=True)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.product.name_eng


class item(models.Model):

    """Item Model."""

    product = models.ForeignKey(product, related_name='product_items')
    supplier = models.ForeignKey(
        supplier, related_name='supplier_items', null=True, blank=True)
    location = models.ForeignKey(
        location, related_name='location_items', null=True, blank=True)
    expire_date = models.DateTimeField(null=True, blank=True)
    unit = models.ForeignKey(unit, related_name="unit_items")
    cost_price = models.FloatField(blank=True, null=True)
    puchase_price = models.FloatField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    sale_price = models.FloatField()
    main_barcode = models.CharField(
        max_length=40, unique=True, blank=True, null=True)
    # supplier_barcode = models.CharField(max_length=40, unique=True)
    item_code = models.CharField(max_length=20, blank=True, null=True)
    sample = models.ImageField(upload_to='item/profile', null=True, blank=True)
    quantity = models.FloatField(default=0)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    # class Meta:
    #     unique_together = ('product', 'supplier', 'expire_date')

    def validate_unique(self, exclude=None):
        """Override Validation Method."""
        if(item.objects.filter(product_id=self.product_id,
                               supplier_id=self.supplier_id,
                               expire_date=self.expire_date).exists()):
            raise 'Product from this supplier at this expiration is dublicated'

    def __unicode__(self):
        """Override Unicode Function."""
        return self.product.name_eng


class barcode(models.Model):

    """Barcode Model."""

    item = models.ForeignKey(item, related_name="item_barcodes")
    owner = models.ForeignKey(Account)
    barcode = models.CharField(max_length=40, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        """Override Unicode Function."""
        return unicode(self.barcode)


class payment(models.Model):

    """Payment Model."""

    name = models.CharField(max_length=40)
    desc = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return 'name= %s' % (self.name)


class doctor(models.Model):

    """Doctor Model."""

    firstname = models.CharField(max_length=40, null=True, blank=True)
    lastname = models.CharField(max_length=40, null=True, blank=True)
    # tagline = models.CharField(max_length=140,null=True, blank=True)
    # is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.firstname


class bill_h(models.Model):

    """Bills Model."""

    items = models.ManyToManyField(
        item, through="bill_d", related_name="item_bill_hs")
    user = models.ForeignKey(Account, related_name="user_bills")
    doctor = models.ForeignKey(
        doctor, null=True, blank=True, related_name="doctor_orders")
    payment = models.ForeignKey(payment, related_name="payment_bills")
    customer = models.ForeignKey(customer, related_name="customer_bills", null=True, blank=True)
    # order_expiration = models.DateTimeField()
    comment = models.CharField(max_length=200, null=True, blank=True)
    saved = models.BooleanField(default=False, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def save(self, *args, **kwargs):
        """Override Save Function."""
        if self.pk:
            with DjTrans.atomic():
                if self.saved:
                    bills = bill_d.objects.filter(bill_id=self.pk)
                    # operation_id = None
                    # if self.case == 1:
                    #     operation_id = 1
                    # elif self.case == 2:
                    #     operation_id = 2
                    # elif self.case == 3:
                    #     operation_id = 3
                    # operation_id = self.operation_id
                    # operation = self.operation.op
                    for bill in bills:
                        item_hist_data = {'item': bill.item,
                                          'quantity': -bill.quantity,
                                          'unit': bill.unit,
                                          'bill_id': bill.id,
                                          'operation_id': 4}
                        create_item_history(item_hist_data)
                super(bill_h, self).save(*args, **kwargs)
                return
        super(bill_h, self).save(*args, **kwargs)

    def __unicode__(self):
        """Override Unicode Function."""
        return str(self.id)


class bill_d(models.Model):

    """Bill Details Model."""

    item = models.ForeignKey(item, related_name="item_bill_ds")
    bill = models.ForeignKey(bill_h, related_name="bill_ds")
    discount = models.FloatField(default=0)
    comment = models.CharField(max_length=200, null=True, blank=True)
    quantity = models.FloatField()
    unit = models.ForeignKey(unit, related_name="unit_bill_ds")
    paid = models.FloatField(default=0)

    @property
    def price(self):
        """Return Price Attr."""
        # *self.unit.transform
        out = (self.item.price-self.item.price*self.discount/100)
        out = out * self.quantity
        return out

    def __unicode__(self):
        """Override Unicode Function."""
        return str(self.item.product.name_eng)


class sal_order_h(models.Model):

    """SaleOrder Model."""

    products = models.ManyToManyField(
        product, through="sal_order_d", related_name="product_order_hs")
    user = models.ForeignKey(Account, related_name="user_sal_orders")
    payment = models.ForeignKey(payment, related_name="payment_sal_orders")
    customer = models.ForeignKey(customer, related_name="customer_sal_orders")
    doctor = models.ForeignKey(
        doctor, null=True, blank=True, related_name="doctor_sal_orders")
    state = models.IntegerField(default=0)
    order_expiration = models.DateTimeField()
    comment = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.id


class sal_order_d(models.Model):

    """Sale Orders Model."""

    product = models.ForeignKey(product, related_name="item_order_ds")
    order = models.ForeignKey(sal_order_h, related_name="order_ds")
    quantity = models.FloatField()
    unit = models.ForeignKey(unit, related_name="unit_order_ds")
    discount = models.FloatField(default=0)
    comment = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.id


class warehouse(models.Model):

    """WareHouses Model."""

    contact_person = models.CharField(max_length=40, null=True, blank=True)
    name_eng = models.CharField(max_length=40)
    name_ar = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=60, null=True, blank=True)
    tel = models.CharField(max_length=20, null=True, blank=True)
    mobile = models.CharField(max_length=20, null=True, blank=True)
    acc_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name_eng


class purchase_request_h(models.Model):

    """Purchase Request Detail Model."""

    products = models.ManyToManyField(
        product,
        through="purchase_request_d",
        related_name="product_request_hs")
    user = models.ForeignKey(Account, related_name="user_purchase_orders")
    state = models.IntegerField(default=0)
    supplier = models.ForeignKey(
        supplier, related_name="supplier_orders", null=True, blank=True)
    sender = models.ForeignKey(
        warehouse, related_name="whid_purchase_request_senders")
    reciver = models.ForeignKey(
        warehouse, related_name="whid_purchase_request_recivers")
    # case = models.IntegerField(default=0)
    # order_expiration = models.DateTimeField()
    comment = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return unicode(self.id)


class purchase_request_d(models.Model):

    """Purchase Request Detail Model."""

    product = models.ForeignKey(product, related_name="product_request_ds")
    purchase_order = models.ForeignKey(
        purchase_request_h, related_name="purchase_orders")
    quantity = models.FloatField(default=0)
    unit = models.ForeignKey(unit, related_name="unit_request_ds")
    prefered_expiration = models.DateTimeField(null=True, blank=True)
    comment = models.CharField(max_length=200, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return unicode(self.product.name_eng)


class operation(models.Model):

    """Operations Model."""

    OP_CHOICES = (
        (u'plus', u'Plus'),
        (u'minus', u'Minus'),
        (u'nutral', u'Nutral'),
    )
    name_eng = models.CharField(max_length=40, null=True, blank=True)
    name_ar = models.CharField(max_length=40, null=True, blank=True)
    op = models.CharField(choices=OP_CHOICES, max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def __unicode__(self):
        """Override Unicode Function."""
        return self.name_eng


class transaction_h(models.Model):

    """TranSaction Model."""

    TRANS_CHOICES = (
        (u'plus', u'Plus'),
        (u'minus', u'Minus'),
        (u'nutral', u'Nutral'),
    )
    items = models.ManyToManyField(
        item, through="transaction_d", related_name="item_transactions_hs")
    title = models.CharField(max_length=100, null=True, blank=True)
    sender = models.ForeignKey(
        warehouse,
        related_name="whid_transaction_senders",
        null=True,
        blank=True)
    reciver = models.ForeignKey(
        warehouse, related_name="whid_transaction_recivers")
    user = models.ForeignKey(Account, related_name="user_transactions")
    # five cases Input=1 output=2 return=3 gard
    operation = models.ForeignKey(
        operation, related_name="operation_transactions")
    comment = models.CharField(max_length=200, blank=True, null=True)
    saved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        """Override Save Function."""
        if self.pk:
            with DjTrans.atomic():
                if self.saved:
                    transactions = transaction_d.objects.filter(
                        transaction_id=self.pk)
                    # operation_id = None
                    # if self.case == 1:
                    #     operation_id = 1
                    # elif self.case == 2:
                    #     operation_id = 2
                    # elif self.case == 3:
                    #     operation_id = 3
                    # operation_id = self.operation_id
                    operation = self.operation.op
                    for trans in transactions:
                        if operation == 'plus':
                            quantity = trans.quantity
                        elif operation == 'minus':
                            quantity = -trans.quantity
                        else:
                            quantity = trans.quantity
                        item_hist_data = {'item': trans.item,
                                          'quantity': quantity, 'unit':
                                          trans.unit,
                                          'transaction_id': trans.id,
                                          'operation': self.operation}
                        create_item_history(item_hist_data)
                super(transaction_h, self).save(*args, **kwargs)
                return
        super(transaction_h, self).save(*args, **kwargs)

    def __unicode__(self):
        """Override Unicode Function."""
        return unicode(self.title)


class transaction_d(models.Model):

    """TranSaction Detail Model."""

    item = models.ForeignKey(item, related_name="item_transactions")
    transaction = models.ForeignKey(transaction_h, related_name="transactions")
    quantity = models.FloatField()
    unit = models.ForeignKey(unit, related_name="unit_transactin_ds")
    cost = models.FloatField(default=0, null=True, blank=True)
    comment = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        """Override Unicode Function."""
        return unicode(str(self.id))

class itemhistory(models.Model):

    """Item History Model to track Item."""

    item = models.ForeignKey(item, related_name='item_histories')
    quantity = models.FloatField(default=0)
    unit = models.ForeignKey(unit, related_name='unit_items_histories')
    transaction = models.ForeignKey(transaction_d, null=True, blank=True)
    bill = models.ForeignKey(bill_d, null=True, blank=True)
    operation = models.ForeignKey(
        operation, related_name='operation_items_history')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,)

    def make_operation(self, num_1, num_2, operation):
        """Return Operation Type."""
        if operation.op == 'plus':
            return num_1+num_2
        elif operation.op == 'minus':
            return num_1+num_2
        elif operation.op == 'nutral':
            return num_1

    def save(self, *args, **kwargs):
        """Override Save Function."""
        with DjTrans.atomic():
            super(itemhistory, self).save(*args, **kwargs)
            item = self.item
            factor = unit_conversion_factor(self.unit, item.unit, item.product)
            item.quantity = item.quantity + self.quantity * factor
            item.save()

    def __unicode__(self):
        """Override Unicode Function."""
        return self.item.product.name_eng


def create_item_history(data):
    """Create Item History."""
    itemhistory.objects.create(**data)

# Unit Conversion Function
def unit_conversion_factor(unit_from, unit_to, product):
    """Return Unit Conversion Factor."""
    factor_from = unitconvertion.objects.filter(unit_from=unit_from).filter(
        product=product).filter(unit_to_id=product.ref_unit_id).get()
    factor_to = unitconvertion.objects.filter(unit_from=unit_to).filter(
        product=product).filter(unit_to_id=product.ref_unit_id).get()
    return factor_from.factor/factor_to.factor
